#include <ros/ros.h>
#include <ros/package.h>

#include <ram_msgs/AdditiveManufacturingTrajectory.h>
#include <ram_trajectory_buffer/UnmodifiedTrajectory.h>

#include <gtest/gtest.h>

std::shared_ptr<ros::NodeHandle> nh;
ros::ServiceClient unmodified_trajectory_client;
ros::Publisher test_trajectory_pub;

TEST(TestSuite, testSrvExistence)
{
  unmodified_trajectory_client = nh->serviceClient<ram_trajectory_buffer::UnmodifiedTrajectory>(
      "ram/buffer/get_unmodified_trajectory");
  bool exists(unmodified_trajectory_client.waitForExistence(ros::Duration(1)));
  EXPECT_TRUE(exists);
}

TEST(TestSuite, testVerifyTrajectoryNotInBuffer)
{
  ros::Time timestamp = ros::Time::now();

  // Service request
  ram_trajectory_buffer::UnmodifiedTrajectory srv;
  srv.request.generated = timestamp;

  bool success(unmodified_trajectory_client.call(srv));
  EXPECT_FALSE(success);
}

TEST(TestSuite, testVerifyTrajectoryInBuffer)
{
  ram_msgs::AdditiveManufacturingTrajectory trajectory;
  ros::Time timestamp = ros::Time::now();
  trajectory.generated = timestamp;
  trajectory.modified = timestamp;
  test_trajectory_pub.publish(trajectory);
  // Service request
  ram_trajectory_buffer::UnmodifiedTrajectory srv;
  srv.request.generated = timestamp;

  bool success(unmodified_trajectory_client.call(srv));
  EXPECT_TRUE(success);
}

int main(int argc,
         char **argv)
{
  ros::init(argc, argv, "buffer_trajectory_test_client");
  nh.reset(new ros::NodeHandle);

  test_trajectory_pub = nh->advertise<ram_msgs::AdditiveManufacturingTrajectory>("ram/trajectory", 5, true);

  // Make sure we have at least one subscriber to this topic
  while (test_trajectory_pub.getNumSubscribers() != 1)
  {
    ROS_WARN_STREAM_THROTTLE(1, test_trajectory_pub.getTopic() + " has zero subscriber, waiting...");
    ros::Duration(0.1).sleep();
  }

  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
