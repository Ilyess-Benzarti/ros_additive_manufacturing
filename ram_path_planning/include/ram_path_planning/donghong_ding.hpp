#ifndef DONGHONDING_HPP
#define DONGHONDING_HPP

#include <future>
#include <mutex>

#include <eigen_conversions/eigen_msg.h>
#include <ram_msgs/AdditiveManufacturingTrajectory.h>
#include <ram_trajectory_files_manager/ram_trajectory_files_manager.hpp>
#include <ros/ros.h>

#include <vtkCenterOfMass.h>
#include <vtkCleanPolyData.h>
#include <vtkDecimatePolylineFilter.h>
#include <vtkLine.h>
#include <vtkPlane.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>

class DonghongDing
{
public:
  // Polygon (possibly multiple contours)
  // At the end: the polygon must contain exactly 1 contour
  typedef vtkSmartPointer<vtkPolyData> Polygon;
  typedef std::vector<Polygon> PolygonVector;
  typedef std::vector<PolygonVector> Layer;

  DonghongDing();

  std::string generateOneLayerTrajectory(const Polygon poly_data,
                                         Layer &layer,
                                         const double deposited_material_width,
                                         const double contours_filtering_tolerance,
                                         const bool is_yaml_file,
                                         const std::array<double, 3> normal_vector = {0, 0, 1},
                                         const double polygon_division_tolerance = M_PI / 6,
                                         const bool closest_to_bisector = false,
                                         const bool use_gui = false);

  std::string generateOneLayerTrajectory(const std::string yaml_file,
                                         Layer &layer,
                                         const double deposited_material_width,
                                         const double contours_filtering_tolerance,
                                         const bool is_yaml_file,
                                         const std::array<double, 3> normal_vector = {0, 0, 1},
                                         const double polygon_division_tolerance = M_PI / 6,
                                         const bool closest_to_bisector = false,
                                         const bool use_gui = false);

  std::string connectMeshLayers(std::vector<Layer> &layers,
                                ram_msgs::AdditiveManufacturingTrajectory &msg);

  void connectYamlLayers(const Layer &current_layer,
                         ram_msgs::AdditiveManufacturingTrajectory &msg,
                         const double number_of_layers,
                         const double height_between_layers,
                         const std::array<double, 3> offset_direction = {0, 0, 1});
  private:
  // FIXME VERSION 2 - Works but unsure if better
  /*
   struct Connection
   {
   vtkIdType polygon_id_1;
   vtkIdType polygon_id_2;
   vtkIdType edge_id_1;
   vtkIdType edge_id_2;
   double p1[3];
   double p2[3];
   double p3[3];
   double p4[3];
   bool second_time; //flag. used for merged the polygons

   };
   */

  const double calculation_tol_ = 1e-6; // In meters

  double deposited_material_width_; // In meters
  double contours_filtering_tolerance_; // In meters
  double polygon_division_tolerance_; // In radians
  bool closest_to_bisector_;

  double normal_vector_[3]; //Normal vector to slicing plane. [0,0,1] in YAML files

  double angleBetweenVectors(const double v1[3],
                             const double v2[3]);

  void computeNormal(vtkPoints *p,
                     double *n);

  bool intersectionBetweenContours(const Polygon poly_data);

  void identifyRelationships(const Polygon poly_data,
                             std::vector<int> &level,
                             std::vector<int> &father);

  bool organizePolygonContoursInLayer(const Polygon poly_data,
                                      const std::vector<int> level,
                                      const std::vector<int> father,
                                      Layer &layer);

  bool findNotch(const Polygon poly_data,
                 vtkIdType &cell_id,
                 vtkIdType &pos,
                 double &angle);

  bool verifyAngles(const Polygon poly_data,
                    const vtkIdType notch_cell_id,
                    const vtkIdType notch_pos,
                    const vtkIdType vertex_cell_id,
                    const vtkIdType vertex_pos);

  bool intersectLineWithContours(const Polygon poly_data,
                                 double point_1[3],
                                 double point_2[3]);

  bool findVertex(const Polygon poly_data,
                  const vtkIdType notch_cell_id,
                  const vtkIdType notch_pos,
                  vtkIdType &vertex_cell_id,
                  vtkIdType &vertex_pos,
                  const double notch_angle);

  bool findIntersectWithBisector(const Polygon poly_data,
                                 const vtkIdType notch_cell_id,
                                 const vtkIdType notch_pos,
                                 vtkIdType &vertex_cell_id,
                                 vtkIdType &vertex_pos,
                                 double vertex[3]);

  bool divideInConvexPolygons(PolygonVector &polygon_source,
                              const int polygon_position,
                              const vtkSmartPointer<vtkPoints> split_points);

  bool removeDuplicatePoints(const Polygon poly_data,
                             const double tolerance = 1e-6);

  bool mergeColinearEdges(const Polygon poly_data,
                          const double tolerance = 1e-6);

  bool offsetPolygonContour(const Polygon poly_data,
                            const double deposited_material_width);

  double identifyZigzagDirection(const Polygon poly_data,
                                 vtkIdType &edge,
                                 vtkIdType &futhest_point);

  void identifyLeftChain(const Polygon poly_data,
                         const vtkIdType edge_id,
                         const vtkIdType opposite_point_id,
                         const vtkSmartPointer<vtkPoints> left_chain,
                         const vtkSmartPointer<vtkPoints> right_chain);

  bool offsetLeftChain(const Polygon poly_data,
                       const vtkIdType edge_id,
                       const vtkIdType opposite_point_id,
                       const vtkSmartPointer<vtkPoints> left_chain,
                       const vtkSmartPointer<vtkPoints> right_chain);

  bool zigzagGeneration(const Polygon poly_data,
                        const vtkIdType edge_id,
                        const vtkIdType opposite_point_id,
                        const vtkSmartPointer<vtkPoints> zigzag_points,
                        const double deposited_material_width);

  void mergeListOfPoints(const Polygon poly_data,
                         const vtkSmartPointer<vtkPoints> left_chain,
                         const vtkSmartPointer<vtkPoints> right_chain,
                         const vtkSmartPointer<vtkPoints> zigzag_points);

  bool mergeConvexPolygons(PolygonVector &polygon_source,
                           const vtkSmartPointer<vtkPoints> split_points,
                           const vtkIdType split_line);

  // VERSION 2 - Works but unsure if better
  // bool mergeConvexPolygons2(Polygons &polygon_source, vtkSmartPointer<vtkPoints> split_points, double d);
  // int findConnectionLine(Polygons polygon_source, std::vector<Connection> &connection_list,vtkIdType current_polygon_id, vtkIdType current_edge_id);

  bool generateTrajectoryInConvexPolygon(const Polygon poly_data);

  void connectLayersWithOnePolygon(std::vector<Layer> &layers,
                                   ram_msgs::AdditiveManufacturingTrajectory &msg,
                                   const unsigned first_layer);

  void divideInLayersWithOnePolygon(std::vector<Layer> &layers,
                                    ram_msgs::AdditiveManufacturingTrajectory &msg,
                                    const unsigned first_layer);

};

#endif
