#include <eigen_conversions/eigen_msg.h>
#include <ram_msgs/AdditiveManufacturingTrajectory.h>
#include <ram_path_planning/GenerateTrajectory.h>
#include <ros/package.h>
#include <ros/ros.h>
#include <ros/package.h>
#include <ram_path_planning/GenerateTrajectory.h>
#include <ram_msgs/AdditiveManufacturingTrajectory.h>
#include <eigen_conversions/eigen_msg.h>

#include <gtest/gtest.h>

std::shared_ptr<ros::NodeHandle> nh;
ros::ServiceClient trajectory_client;
bool use_gui;

TEST(TestSuite, testSrvExistence)
{
  trajectory_client = nh->serviceClient<ram_path_planning::GenerateTrajectory>("ram/path_planning/generate_trajectory");
  bool exists(trajectory_client.waitForExistence(ros::Duration(1)));
  EXPECT_TRUE(exists);
}

/// YAML

TEST(TestSuite, testPolygonWithinternalContours)
{
  ram_path_planning::GenerateTrajectory srv;
  srv.request.generation_params.file = ros::package::getPath("ram_path_planning") + "/yaml/2_polygons_4_contours.yaml";
  srv.request.generation_params.deposited_material_width = 1e-3;
  srv.request.generation_params.contours_filtering_tolerance = 0.25e-3;
  srv.request.generation_params.height_between_layers = 2e-3;
  srv.request.generation_params.number_of_layers = 2;
  bool success(trajectory_client.call(srv));
  EXPECT_TRUE(success && srv.response.error_msg.empty());
}

TEST(TestSuite, testIntersectedPolygons)
{
  ram_path_planning::GenerateTrajectory srv;
  srv.request.generation_params.file = ros::package::getPath("ram_path_planning") + "/yaml/polygon_intersection.yaml";
  srv.request.generation_params.deposited_material_width = 1e-3;
  srv.request.generation_params.contours_filtering_tolerance = 0.25e-3;
  srv.request.generation_params.height_between_layers = 0.001;
  srv.request.generation_params.number_of_layers = 2;
  bool success(trajectory_client.call(srv));
  EXPECT_FALSE(success && srv.response.error_msg.empty()); // Should fail because polygons intersect
}

TEST(TestSuite, testConvexPolygon)
{
  ram_path_planning::GenerateTrajectory srv;
  srv.request.generation_params.file = ros::package::getPath("ram_path_planning") + "/yaml/round_rectangle.yaml";
  srv.request.generation_params.deposited_material_width = 1e-3;
  srv.request.generation_params.contours_filtering_tolerance = 0.25e-3;
  srv.request.generation_params.height_between_layers = 2e-3;
  srv.request.generation_params.number_of_layers = 2;
  bool success(trajectory_client.call(srv));
  EXPECT_TRUE(success && srv.response.error_msg.empty());
}

TEST(TestSuite, testConcavePolygonBig)
{
  ram_path_planning::GenerateTrajectory srv;
  srv.request.generation_params.file = ros::package::getPath("ram_path_planning") + "/yaml/concave_4_contours_big.yaml";
  srv.request.generation_params.deposited_material_width = 0.1;
  srv.request.generation_params.contours_filtering_tolerance = 0.5e-3;
  srv.request.generation_params.height_between_layers = 0.1;
  srv.request.generation_params.number_of_layers = 2;
  bool success(trajectory_client.call(srv));
  EXPECT_TRUE(success && srv.response.error_msg.empty());
}

TEST(TestSuite, testConcavePolygonSmall)
{
  ram_path_planning::GenerateTrajectory srv;
  srv.request.generation_params.file = ros::package::getPath("ram_path_planning") + "/yaml/concave_4_contours_small.yaml";
  srv.request.generation_params.deposited_material_width = 1e-3;
  srv.request.generation_params.contours_filtering_tolerance = 0.25e-5;
  srv.request.generation_params.height_between_layers = 0.1;
  srv.request.generation_params.number_of_layers = 2;
  bool success(trajectory_client.call(srv));
  EXPECT_TRUE(success && srv.response.error_msg.empty());
}

TEST(TestSuite, testStar)
{
  ram_path_planning::GenerateTrajectory srv;
  srv.request.generation_params.file = ros::package::getPath("ram_path_planning") + "/yaml/star.yaml";
  srv.request.generation_params.deposited_material_width = 1e-3;
  srv.request.generation_params.contours_filtering_tolerance = 0.25e-3;
  srv.request.generation_params.height_between_layers = 0.1;
  srv.request.generation_params.number_of_layers = 2;
  bool success(trajectory_client.call(srv));
  EXPECT_TRUE(success && srv.response.error_msg.empty());
}

/// MESHES

TEST(TestSuite, testSTLFileTwistedPyramid)
{
  ram_path_planning::GenerateTrajectory srv;
  srv.request.generation_params.file = ros::package::getPath("ram_path_planning") + "/meshes/twisted_pyramid.stl";
  srv.request.generation_params.deposited_material_width = 1e-3;
  srv.request.generation_params.contours_filtering_tolerance = 1e-3;
  srv.request.generation_params.height_between_layers = 5e-3;
  bool success(trajectory_client.call(srv));
  EXPECT_TRUE(success && srv.response.error_msg.empty());
}

TEST(TestSuite, testPLYFileTwistedPyramid)
{
  ram_path_planning::GenerateTrajectory srv;
  srv.request.generation_params.file = ros::package::getPath("ram_path_planning") + "/meshes/twisted_pyramid.ply";
  srv.request.generation_params.deposited_material_width = 0.001;
  srv.request.generation_params.contours_filtering_tolerance = 0.5 * 1e-3;
  srv.request.generation_params.height_between_layers = 0.005;
  bool success(trajectory_client.call(srv));
  EXPECT_TRUE(success && srv.response.error_msg.empty());
}

TEST(TestSuite, testOBJFileTwistedPyramid)
{
  ram_path_planning::GenerateTrajectory srv;
  srv.request.generation_params.file = ros::package::getPath("ram_path_planning") + "/meshes/twisted_pyramid.obj";
  srv.request.generation_params.deposited_material_width = 0.001;
  srv.request.generation_params.contours_filtering_tolerance = 0.005 * 1e-3;
  srv.request.generation_params.height_between_layers = 0.005;
  bool success(trajectory_client.call(srv));
  EXPECT_TRUE(success && srv.response.error_msg.empty());
}

TEST(TestSuite, testPLYFileTwoTwistedPyramids)
{
  ram_path_planning::GenerateTrajectory srv;
  srv.request.generation_params.file = ros::package::getPath("ram_path_planning") + "/meshes/two_twisted_pyramids.ply";
  srv.request.generation_params.deposited_material_width = 1e-3;
  srv.request.generation_params.contours_filtering_tolerance = 0.25e-3;
  srv.request.generation_params.height_between_layers = 5e-3;
  bool success(trajectory_client.call(srv));
  EXPECT_FALSE(success && srv.response.error_msg.empty()); // Not implemented yet!
}

TEST(TestSuite, testConeTruncated)
{
  ram_path_planning::GenerateTrajectory srv;
  srv.request.generation_params.file = ros::package::getPath("ram_path_planning") + "/meshes/cone_truncated.ply";
  srv.request.generation_params.deposited_material_width = 0.001;
  srv.request.generation_params.contours_filtering_tolerance = 0.6 * 1e-3;
  srv.request.generation_params.height_between_layers = 0.005;
  bool success(trajectory_client.call(srv));
  EXPECT_TRUE(success && srv.response.error_msg.empty());
}

TEST(TestSuite, testDome)
{
  ram_path_planning::GenerateTrajectory srv;
  srv.request.generation_params.file = ros::package::getPath("ram_path_planning") + "/meshes/dome.ply";
  srv.request.generation_params.deposited_material_width = 0.001;
  srv.request.generation_params.contours_filtering_tolerance = 0.5 * 1e-3;
  srv.request.generation_params.height_between_layers = 0.002;
  bool success(trajectory_client.call(srv));
  EXPECT_TRUE(success && srv.response.error_msg.empty());
}

TEST(TestSuite, testInversedPyramid)
{
  ram_path_planning::GenerateTrajectory srv;
  srv.request.generation_params.file = ros::package::getPath("ram_path_planning") + "/meshes/inversed_pyramid.ply";
  srv.request.generation_params.deposited_material_width = 0.001;
  srv.request.generation_params.contours_filtering_tolerance = 0;
  srv.request.generation_params.height_between_layers = 0.005;
  bool success(trajectory_client.call(srv));
  EXPECT_TRUE(success && srv.response.error_msg.empty());
}

TEST(TestSuite, testSTLCubeNonZSlicing)
{
  ram_path_planning::GenerateTrajectory srv;
  srv.request.generation_params.file = ros::package::getPath("ram_path_planning") + "/meshes/cube.stl";
  srv.request.generation_params.deposited_material_width = 1e-3;
  srv.request.generation_params.contours_filtering_tolerance = 1e-3;
  srv.request.generation_params.height_between_layers = 5e-3;
  // Slicing vector is not Z:
  srv.request.generation_params.slicing_direction.x = -0.15;
  srv.request.generation_params.slicing_direction.y = -0.2;
  srv.request.generation_params.slicing_direction.z = 1;
  bool success(trajectory_client.call(srv));
  EXPECT_TRUE(success && srv.response.error_msg.empty());

  if (use_gui)
    ROS_ERROR_STREAM("PLEASE CLOSE THE VTK WINDOW TO TERMINATE");
}

int main(int argc,
         char **argv)
{
  ros::init(argc, argv, "path_planning_test_client");
  nh.reset(new ros::NodeHandle);

  nh->param<bool>("use_gui", use_gui, false);
  if (use_gui)
    ROS_ERROR_STREAM("Press 'enter' in the terminal to go to the next step");

  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
