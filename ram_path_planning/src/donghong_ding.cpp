#include <ram_path_planning/donghong_ding.hpp>

class Semaphore
{
private:
  unsigned int counter_;
  std::mutex mutex_;
  std::condition_variable condition_;

public:
  inline Semaphore(unsigned int counter) :
          counter_(counter)
  {
  }

  inline void wait()
  {
    std::unique_lock<std::mutex> lock(mutex_);
    condition_.wait(lock, [&]()->bool
    { return counter_>0;});
    --counter_;
  }

  inline void signal()
  {
    std::unique_lock<std::mutex> lock(mutex_);
    ++counter_;
    condition_.notify_one();
  }
};

// Allow 4 concurrent threads, tweak depending on the machine you use
Semaphore semaphore(4);

DonghongDing::DonghongDing() :
        deposited_material_width_(0.005), // 5 millimeters
        contours_filtering_tolerance_(0.0025), // 2.5 millimeters
        polygon_division_tolerance_(M_PI / 6), // 30 degrees
        closest_to_bisector_(false)
{
}

std::string DonghongDing::generateOneLayerTrajectory(const Polygon poly_data,
                                                     Layer &layer,
                                                     const double deposited_material_width,
                                                     const double contours_filtering_tolerance,
                                                     const bool is_yaml_file,
                                                     const std::array<double, 3> normal_vector,
                                                     const double polygon_division_tolerance,
                                                     const bool closest_to_bisector,
                                                     const bool use_gui)
{
  if (!poly_data)
    return "generateOneLayerTrajectory: polydata is not initialized";

  if (poly_data->GetNumberOfPoints() == 0)
    return "generateOneLayerTrajectory: polydata is empty";

  if (is_yaml_file)
  {
    normal_vector_[0] = 0;
    normal_vector_[1] = 0;
    normal_vector_[2] = 1;
  }
  else //Mesh file. Normal vector = slicing vector
  {
    normal_vector_[0] = normal_vector[0];
    normal_vector_[1] = normal_vector[1];
    normal_vector_[2] = normal_vector[2];

    vtkMath::Normalize(normal_vector_);

    if (normal_vector[0] == 0 && normal_vector[1] == 0 && normal_vector[2] == 0)
    {
      normal_vector_[0] = 0;
      normal_vector_[1] = 0;
      normal_vector_[2] = 1;
    }
  }

  deposited_material_width_ = deposited_material_width;
  polygon_division_tolerance_ = polygon_division_tolerance;
  contours_filtering_tolerance_ = contours_filtering_tolerance;
  closest_to_bisector_ = closest_to_bisector;

  // FIXME Is this useful? What should be the tolerance?
  if (!removeDuplicatePoints(poly_data, deposited_material_width_ / 2)) // tolerance default
    return "Failed to remove duplicate points";

  if (!mergeColinearEdges(poly_data, contours_filtering_tolerance_))
    return "Failed to merge colinear edges";

  if (intersectionBetweenContours(poly_data))
    return "Contours intersects";

  std::vector<int> level;
  std::vector<int> father;
  identifyRelationships(poly_data, level, father);
  if (!organizePolygonContoursInLayer(poly_data, level, father, layer))
    return "Failed to organize polygon contours in layer";

  // Divide in convex polygons
  vtkSmartPointer<vtkPoints> split_points = vtkSmartPointer<vtkPoints>::New();
  for (unsigned i(0); i < layer.size(); ++i)
  {
    unsigned j = 0;
    while (j < layer[i].size())
    {

      if (use_gui)
      {
        std::string we_dont_care;
        std::getline(std::cin, we_dont_care);
        ROS_INFO_STREAM("Enter was pressed: divide in convex polygons");
      }

      if (divideInConvexPolygons(layer[i], j, split_points))
      {
        vtkIdType n_cells = layer[i][j]->GetNumberOfCells();
        if (n_cells == 1 && vtkPolygon::IsConvex(layer[i][j]->GetPoints()))
          j++;
      }
      else
        return "generateOneLayerTrajectory: Error dividing the polygons";
    }
  }

  if (use_gui)
  {
    std::string we_dont_care;
    std::getline(std::cin, we_dont_care);
    ROS_INFO_STREAM("Enter was pressed: zigzag in convex polygons");
  }

  // Path generation in convex polygons
  std::vector<std::future<bool> > futures;
  for (auto polygons : layer)
    for (auto poly_data : polygons)
      futures.push_back(std::async(&DonghongDing::generateTrajectoryInConvexPolygon, this, poly_data));

  bool global_return = true;
  for (auto &t : futures)
    global_return &= t.get();
  if (!global_return)
    return "Failed to generate trajectory in one of the convex polygons";

  // Merge convex polygons
  /*
   // Merge convex polygons. Second method
   vtkIdType id = 0;
   for (unsigned i(0); i < polygon_vector.size(); i++)
   {
   vtkIdType lines_in_polygon = polygon_vector[i].size() - 1;
   if (lines_in_polygon != 0)
   {
   vtkSmartPointer<vtkPoints> local_points = vtkSmartPointer<vtkPoints>::New();
   for (vtkIdType j(0); j < (lines_in_polygon * 2); ++j)
   {
   double p[3];
   split_points->GetPoint(id, p);
   local_points->InsertNextPoint(p);
   id++;
   }
   mergeConvexPolygons2(polygon_vector[i], local_points);
   }
   }
   */
  // Merge convex polygons. First method
  vtkIdType n_lines = split_points->GetNumberOfPoints() / 2;
  unsigned polygon_id = layer.size() - 1;
  for (int line_id(n_lines - 1); line_id >= 0; --line_id)  // LIFO
  {

    if (use_gui)
    {
      std::string we_dont_care;
      std::getline(std::cin, we_dont_care);
      ROS_INFO_STREAM("Enter was pressed: merge polygons. Line " << line_id);
    }

    if (layer[polygon_id].size() == 1)
      polygon_id--;
    if (!mergeConvexPolygons(layer[polygon_id], split_points, line_id))
      return "Failed to merge convex polygons";
  }

  for (auto polygons : layer)
    for (auto poly_data : polygons)
    {
      if (!removeDuplicatePoints(poly_data))
        return "Failed to remove duplicate points";
      if (!mergeColinearEdges(poly_data))
        return "Failed to merge colinear edges";
    }

  if (use_gui)
  {
    std::string we_dont_care;
    ROS_INFO_STREAM("Path generated on one layer");
    std::getline(std::cin, we_dont_care);
  }
  return "";
}

std::string DonghongDing::generateOneLayerTrajectory(const std::string yaml_file,
                                                     Layer &layer,
                                                     const double deposited_material_width,
                                                     const double contours_filtering_tolerance,
                                                     const bool is_yaml_file,
                                                     const std::array<double, 3> normal_vector,
                                                     const double polygon_division_tolerance,
                                                     const bool closest_to_bisector,
                                                     const bool use_gui)
{
  // Prepare contours
  const Polygon poly_data = Polygon::New();
  if (!TrajectoryFilesManager::yamlFileToPolydata(yaml_file, poly_data))
    return "Could not parse the YAML file";

  layer.clear();
  return generateOneLayerTrajectory(poly_data, layer, deposited_material_width, contours_filtering_tolerance,
                                    is_yaml_file,
                                    normal_vector,
                                    polygon_division_tolerance,
                                    closest_to_bisector, use_gui);
}

double DonghongDing::angleBetweenVectors(const double v1[3],
                                         const double v2[3])
{
  double norm = vtkMath::Norm(v1) * vtkMath::Norm(v2);

  double cross_v1_v2[3];
  vtkMath::Cross(v1, v2, cross_v1_v2);
  double sin_angle = vtkMath::Dot(normal_vector_, cross_v1_v2) / norm;
  double cos_angle = vtkMath::Dot(v1, v2) / norm;
  return atan2(sin_angle, cos_angle);
}

void DonghongDing::computeNormal(vtkPoints *p,
                                 double *n)
{
  double ax, ay, az, bx, by, bz;

  vtkIdType n_points = p->GetNumberOfPoints();
  double p0[3];
  p->GetPoint(0, p0);
  n[0] = 0;
  n[1] = 0;
  n[2] = 0;
  for (vtkIdType i = 0; i < n_points; ++i)
  {
    double v1[3];
    p->GetPoint(i, v1);
    double v2[3];
    p->GetPoint((i + 1) % n_points, v2);
    ax = v1[0] - p0[0];
    ay = v1[1] - p0[1];
    az = v1[2] - p0[2];

    bx = v2[0] - p0[0];
    by = v2[1] - p0[1];
    bz = v2[2] - p0[2];

    // Cross Product
    n[0] += (ay * bz - az * by);
    n[1] += (az * bx - ax * bz);
    n[2] += (ax * by - ay * bx);

  }
  vtkMath::Normalize(n);
}

bool DonghongDing::intersectionBetweenContours(const Polygon poly_data)
{
  vtkIdType n_cells = poly_data->GetNumberOfCells(); // Numbers of cells in the polyData

  for (vtkIdType i(0); i < n_cells; ++i)
  {
    for (vtkIdType j(i + 1); j < n_cells; ++j)
    {
      //intersection between i contour and the (i+1=j) contour
      vtkIdType n_edges_i = poly_data->GetCell(i)->GetNumberOfEdges();
      vtkIdType n_edges_j = poly_data->GetCell(j)->GetNumberOfEdges();
      for (vtkIdType e_i(0); e_i < n_edges_i; ++e_i)
      {
        for (vtkIdType e_j(0); e_j < n_edges_j; ++e_j)
        {
          // Get the two points of the edge i
          double ei_p1[3];
          double ei_p2[3];
          poly_data->GetCell(i)->GetEdge(e_i)->GetPoints()->GetPoint(0, ei_p1);
          poly_data->GetCell(i)->GetEdge(e_i)->GetPoints()->GetPoint(1, ei_p2);
          // Get the two points of the edge j
          double ej_p1[3];
          double ej_p2[3];
          poly_data->GetCell(j)->GetEdge(e_j)->GetPoints()->GetPoint(0, ej_p1);
          poly_data->GetCell(j)->GetEdge(e_j)->GetPoints()->GetPoint(1, ej_p2);

          double u; //  Parametric coordinate of the line 1
          double v; //  Parametric coordinate of the line 2
          int intersection = vtkLine::Intersection3D(ei_p1, ei_p2, ej_p1, ej_p2, u, v);

          if (intersection == 2)
          {
            ROS_ERROR_STREAM("intersectionBetweenContours: contours " << i << " and " << j << " intersects");
            return true;
          }
          else if (intersection == 3)
          {
            // Overlapping = error
            double closest_pt1[3];
            double closest_pt2[3];
            if (vtkLine::DistanceBetweenLineSegments(ei_p1, ei_p2, ej_p1, ej_p2, closest_pt1, closest_pt2, u, v) == 0)
            {
              ROS_ERROR_STREAM("intersectionBetweenContours: contours " << i << " and " << j << " are overlapping");
              return true;
            }
          }
        }
      }
    }
  }
  return false;
}

void DonghongDing::identifyRelationships(const Polygon poly_data,
                                         std::vector<int> &level,
                                         std::vector<int> &father)
{
  vtkIdType n_cells = poly_data->GetNumberOfCells();

  //relationship matrix: if the j cell is inside i cell, relationship[i][j]=1
  std::vector<std::vector<int> > relationship(n_cells, std::vector<int>(n_cells, 0));

  level = std::vector<int>(n_cells, 0);
  father = std::vector<int>(n_cells, -1);

  for (vtkIdType i(0); i < n_cells; ++i)
  {
    // Points in i cell
    vtkSmartPointer<vtkPoints> point_list = vtkSmartPointer<vtkPoints>::New();
    point_list = poly_data->GetCell(i)->GetPoints();

    //Create the polygon with the points in "point_list"
    vtkSmartPointer<vtkPolygon> polygon = vtkSmartPointer<vtkPolygon>::New();
    for (vtkIdType k(0); k < point_list->GetNumberOfPoints(); ++k)
    {
      double p[3];
      point_list->GetPoint(k, p);
      polygon->GetPoints()->InsertNextPoint(p[0], p[1], p[2]);
    }
    // arguments of the PointInPolygon function
    double n[3];
    computeNormal(polygon->GetPoints(), n);
    double bounds[6];
    polygon->GetPoints()->GetBounds(bounds);

    for (vtkIdType j(0); j < n_cells; ++j)
    {
      if (i != j)
      {
        //first point in the j cell
        double p[3];
        poly_data->GetCell(j)->GetPoints()->GetPoint(0, p);
        int in_polygon = polygon->PointInPolygon(
            p, polygon->GetPoints()->GetNumberOfPoints(),
            static_cast<double*>(polygon->GetPoints()->GetData()->GetVoidPointer(0)),
            bounds, n); //
        if (in_polygon)
        {
          level[j]++; // level of the polygon in the depth-tree
          relationship[i][j] = 1;
        }
      }
    }
  }
  //find the father of each contour
  for (unsigned j(0); j < n_cells; ++j)
  {
    for (unsigned i(0); i < n_cells; ++i)
    {
      if (relationship[i][j] == 1 && level[i] == (level[j] - 1)) // the level of the father cell is
      {
        father[j] = i;
        break;
      }
    }
  }

}

bool DonghongDing::organizePolygonContoursInLayer(const Polygon poly_data,
                                                  const std::vector<int> level,
                                                  const std::vector<int> father,
                                                  Layer &layer)
{
  typedef vtkSmartPointer<vtkPolygon> Contour;

  if (!poly_data)
  {
    ROS_ERROR_STREAM("organizePolygonContoursInLayer: poly_data is not initialized!");
    return false;
  }

  vtkIdType n_cells_contours = poly_data->GetNumberOfCells();
  if (n_cells_contours == 0)
  {
    ROS_ERROR_STREAM("organizePolygonContoursInLayer: poly_data is empty!");
    return false;
  }

  layer.clear(); // We might have deleted poly_data if it belonged to layer

  for (vtkIdType i(0); i < n_cells_contours; ++i)
  {
    if (level[i] % 2 != 0)
      continue;

    vtkSmartPointer<vtkCellArray> contour_array = vtkSmartPointer<vtkCellArray>::New();
    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
    //save the father
    Contour contour = Contour::New();
    vtkSmartPointer<vtkPoints> point_list = vtkSmartPointer<vtkPoints>::New();
    point_list = poly_data->GetCell(i)->GetPoints(); // Points of the i cell

    // Copy all points in the new contour
    double n[3];
    computeNormal(point_list, n); // Contour orientation

    // Contour is not in the XY plane
    double angle = vtkMath::AngleBetweenVectors(n, normal_vector_);
    if (sin(angle) > calculation_tol_)
    {
      ROS_ERROR_STREAM("organizePolygonContoursInLayer: Contour is not co-planar to slicing plane ");
      return false;
    }

    if (std::abs(cos(angle) + 1) < calculation_tol_) // Clockwise direction
    {
      for (vtkIdType k(0); k < point_list->GetNumberOfPoints(); ++k)
      {
        double p[3];
        point_list->GetPoint(k, p);
        contour->GetPointIds()->InsertNextId(points->GetNumberOfPoints());
        points->InsertNextPoint(p);
      }
    }
    else if (std::abs(cos(angle) - 1) < calculation_tol_) // Counter-clockwise direction. Reverse order
    {
      for (vtkIdType k = point_list->GetNumberOfPoints(); k > 0; --k)
      {
        double p[3];
        point_list->GetPoint((k - 1), p);
        contour->GetPointIds()->InsertNextId(points->GetNumberOfPoints());
        points->InsertNextPoint(p);
      }
    }

    contour_array->InsertNextCell(contour);

    // Process children of the contour we just have re-ordered
    for (vtkIdType j(0); j < n_cells_contours; ++j)
    {
      if (father[j] != i)
        continue;
      //save children (father[j] == i)
      contour = vtkSmartPointer<vtkPolygon>::New();
      point_list = vtkSmartPointer<vtkPoints>::New();
      point_list = poly_data->GetCell(j)->GetPoints(); // Points of the j-th cell
      //copy all points in the new contour
      double n[3];
      computeNormal(point_list, n);          // polygon orientation
      double angle = vtkMath::AngleBetweenVectors(n, normal_vector_);
      if (sin(angle) > calculation_tol_)
      {
        ROS_ERROR_STREAM("organizePolygonContoursInLayer: Contour is not co-planar to slicing plane ");
        return false;
      }
      if (std::abs(cos(angle) - 1) < calculation_tol_)          //counter-clockwise direction
      {
        for (vtkIdType k = 0; k < point_list->GetNumberOfPoints(); k++)
        {
          double p[3];
          point_list->GetPoint(k, p);
          contour->GetPointIds()->InsertNextId(points->GetNumberOfPoints());
          points->InsertNextPoint(p[0], p[1], p[2]);
        }
      }
      else if (std::abs(cos(angle) + 1) < calculation_tol_) //clockwise direction. Change order
      {
        for (vtkIdType k = point_list->GetNumberOfPoints(); k > 0; k--)
        {
          double p[3];
          point_list->GetPoint((k - 1), p);
          contour->GetPointIds()->InsertNextId(points->GetNumberOfPoints());
          points->InsertNextPoint(p);
        }
      }
      else
      {
        ROS_ERROR_STREAM("organizePolygonContoursInLayer: normal vector is not parallel to Z axis");
        return false;
      }
      contour_array->InsertNextCell(contour);

    }

    // Make polygon
    Polygon polygon = Polygon::New();
    PolygonVector poly_vector;
    polygon->SetPolys(contour_array); // polygon_array contains the contours
    polygon->SetPoints(points);
    poly_vector.push_back(polygon);
    layer.push_back(poly_vector);
  }

  return true;
}

bool DonghongDing::findNotch(const Polygon poly_data,
                             vtkIdType &cell_id,
                             vtkIdType &pos,
                             double &angle)
{
  vtkIdType n_cells = poly_data->GetNumberOfCells();

  for (vtkIdType i(0); i < n_cells; ++i)
  {
    // Points in the cell i
    vtkIdType n_points = poly_data->GetCell(i)->GetNumberOfPoints();
    for (vtkIdType j(0); j < n_points; ++j)
    {
      double p0[3];
      double p1[3]; // Central point
      double p2[3];

      if (j == 0)
        poly_data->GetCell(i)->GetPoints()->GetPoint((n_points - 1), p0);
      else
        poly_data->GetCell(i)->GetPoints()->GetPoint((j - 1), p0);

      poly_data->GetCell(i)->GetPoints()->GetPoint(j, p1);
      poly_data->GetCell(i)->GetPoints()->GetPoint((j + 1) % n_points, p2);

      double v1[3]; // Vector between p1 and p0
      double v2[3]; // Vector between p1 and p2

      vtkMath::Subtract(p0, p1, v1);
      vtkMath::Subtract(p2, p1, v2);

      angle = angleBetweenVectors(v1, v2);
      if (angle < 0) // Found notch
      {
        cell_id = i;
        pos = j;
        return true;
      }
    }
  }

  ROS_ERROR_STREAM("findNotch: Zero notch found");
  return false;
}

bool DonghongDing::verifyAngles(const Polygon poly_data,
                                const vtkIdType notch_cell_id,
                                const vtkIdType notch_pos,
                                const vtkIdType vertex_cell_id,
                                const vtkIdType vertex_pos)
{
  vtkIdType n_points_cell;
  double notch_prev[3];
  double notch[3];
  double notch_next[3]; // notch next point int the polygon
  n_points_cell = poly_data->GetCell(notch_cell_id)->GetNumberOfPoints();

  if (notch_pos == 0)
    poly_data->GetCell(notch_cell_id)->GetPoints()->GetPoint((n_points_cell - 1), notch_prev);
  else
    poly_data->GetCell(notch_cell_id)->GetPoints()->GetPoint((notch_pos - 1), notch_prev);
  poly_data->GetCell(notch_cell_id)->GetPoints()->GetPoint(notch_pos, notch);
  poly_data->GetCell(notch_cell_id)->GetPoints()->GetPoint((notch_pos + 1) % n_points_cell, notch_next);

  double vertex_prev[3];
  double vertex[3];
  double vertex_next[3]; // vertex next point int the polygon
  n_points_cell = poly_data->GetCell(vertex_cell_id)->GetNumberOfPoints();

  if (vertex_pos == 0)
    poly_data->GetCell(vertex_cell_id)->GetPoints()->GetPoint((n_points_cell - 1), vertex_prev);
  else
    poly_data->GetCell(vertex_cell_id)->GetPoints()->GetPoint((vertex_pos - 1), vertex_prev);
  poly_data->GetCell(vertex_cell_id)->GetPoints()->GetPoint(vertex_pos, vertex);
  poly_data->GetCell(vertex_cell_id)->GetPoints()->GetPoint((vertex_pos + 1) % n_points_cell, vertex_next);

  // angles
  double v_1[3];
  double v_2[3];
  //angle 1: between the lines notch - notch_prev and notch - vertex
  vtkMath::Subtract(vertex, notch, v_2);
  vtkMath::Subtract(notch_prev, notch, v_1);
  if (vtkMath::AngleBetweenVectors(v_1, v_2) < polygon_division_tolerance_)
    return false;
  //angle 2: between the lines notch - notch_next and notch - vertex
  vtkMath::Subtract(notch_next, notch, v_1);
  if (vtkMath::AngleBetweenVectors(v_1, v_2) < polygon_division_tolerance_)
    return false;
  //angle 3: between the lines vertex - vertex_prev and notch - vertex
  vtkMath::Subtract(notch, vertex, v_2);
  vtkMath::Subtract(vertex_prev, vertex, v_1);
  if (vtkMath::AngleBetweenVectors(v_1, v_2) < polygon_division_tolerance_)
    return false;
  //angle 4: between the lines vertex - vertex_next and notch - vertex
  vtkMath::Subtract(vertex_next, vertex, v_1);
  if (vtkMath::AngleBetweenVectors(v_1, v_2) < polygon_division_tolerance_)
    return false;
  return true;
}

bool DonghongDing::intersectLineWithContours(const Polygon poly_data,
                                             double point_1[3],
                                             double point_2[3])
{
  vtkIdType n_cells = poly_data->GetNumberOfCells();
  for (vtkIdType cell_id(0); cell_id < n_cells; ++cell_id) //verify intersections with the geometries
  {
    vtkIdType n_edges = poly_data->GetCell(cell_id)->GetNumberOfEdges();
    for (vtkIdType edge_id(0); edge_id < n_edges; ++edge_id)
    {
      double e_p1[3];
      double e_p2[3];
      poly_data->GetCell(cell_id)->GetEdge(edge_id)->GetPoints()->GetPoint(0, e_p1);
      poly_data->GetCell(cell_id)->GetEdge(edge_id)->GetPoints()->GetPoint(1, e_p2);
      double u;
      double v;
      int intersection = 0;
      //intersect the line "notch  p_1" with the edge(line e_p1  e_p2)
      intersection = vtkLine::Intersection3D(point_1, point_2, e_p1, e_p2, u, v);
      if (intersection == 2 && std::abs(u) > calculation_tol_ && std::abs(u - 1) > calculation_tol_) // u!= 0 &&0u!=1
        return true;
    }
  }
  return false;
}

bool DonghongDing::findVertex(const Polygon poly_data,
                              const vtkIdType notch_cell_id,
                              const vtkIdType notch_pos,
                              vtkIdType &vertex_cell_id,
                              vtkIdType &vertex_pos,
                              const double notch_angle)
{
  double ref_angle = notch_angle * (-1);
  vtkIdType n_cells = poly_data->GetNumberOfCells();
  bool is_vertex = false;

  //2.1. reference vectors
  double v_ref[3];

  double notch[3];
  double notch_next[3]; // notch next point int the polygon
  vtkIdType n_points_cell = poly_data->GetCell(notch_cell_id)->GetNumberOfPoints();
  poly_data->GetCell(notch_cell_id)->GetPoints()->GetPoint(notch_pos, notch);
  poly_data->GetCell(notch_cell_id)->GetPoints()->GetPoint((notch_pos + 1) % n_points_cell, notch_next);
  vtkMath::Subtract(notch, notch_next, v_ref); //vector between p2 and p1. reference vector

  //2.2. Determine whether the points are inside the zone
  bool is_notch = false; //Flag. change if the the possible second point is a notch
  float vertex_d = FLT_MAX;
  for (vtkIdType cell_id(0); cell_id < n_cells; ++cell_id)
  {
    vtkIdType n_points = poly_data->GetCell(cell_id)->GetNumberOfPoints();
    for (vtkIdType point_id(0); point_id < n_points; ++point_id)
    {
      double p0[3];
      double p[3]; // Central point
      double p2[3];
      if (point_id == 0)
        poly_data->GetCell(cell_id)->GetPoints()->GetPoint((n_points - 1), p0);
      else
        poly_data->GetCell(cell_id)->GetPoints()->GetPoint((point_id - 1), p0);
      poly_data->GetCell(cell_id)->GetPoints()->GetPoint(point_id, p);
      poly_data->GetCell(cell_id)->GetPoints()->GetPoint((point_id + 1) % n_points, p2);

      double vector[3]; // Vector between the notch and the possible second point of the line
      vtkMath::Subtract(p, notch, vector);
      double angle = angleBetweenVectors(v_ref, vector); // Calculate angle

      if (!(angle >= 0 && angle <= ref_angle))  // Point is not inside the reference area
        continue;
      // Measure and verify intersections with the geometries
      if (intersectLineWithContours(poly_data, notch, p))
        continue;

      // Verify if the point is a notch
      double p1_p0[3]; // Vector between p1 and p0
      double p2_p0[3]; // Vector between p1 and p2
      vtkMath::Subtract(p0, p, p1_p0);
      vtkMath::Subtract(p2, p, p2_p0);
      double point_angle = angleBetweenVectors(p1_p0, p2_p0);

      // Verify the flag
      if (point_angle < 0)
        if (!is_notch)
        {
          vertex_d = FLT_MAX;
          is_notch = true;
        }
      if (!((point_angle > 0 && !is_notch) || point_angle < 0))
        continue;

      double point_d; // Distance between the notch and p

      // Compute distance between the notch and p1
      if (closest_to_bisector_)
        point_d = sqrt(vtkMath::Distance2BetweenPoints(notch, p)) * std::abs(sin(angle - ref_angle / 2));
      else
        point_d = sqrt(vtkMath::Distance2BetweenPoints(notch, p));

      if (point_d < vertex_d)
      {
        // Save point
        vertex_d = point_d;
        is_vertex = true;
        vertex_pos = point_id;
        vertex_cell_id = cell_id;

      }
    }
  }

  return is_vertex;
}

bool DonghongDing::findIntersectWithBisector(const Polygon poly_data,
                                             const vtkIdType notch_cell_id,
                                             const vtkIdType notch_pos,
                                             vtkIdType &vertex_cell_id,
                                             vtkIdType &vertex_pos,
                                             double vertex[3])
{
  double notch_prev[3];
  double notch[3]; // Central point
  double notch_next[3];

  vtkIdType n_points_cell = poly_data->GetCell(notch_cell_id)->GetNumberOfPoints();

  if (notch_pos == 0)
    poly_data->GetCell(notch_cell_id)->GetPoints()->GetPoint((n_points_cell - 1), notch_prev);
  else
    poly_data->GetCell(notch_cell_id)->GetPoints()->GetPoint((notch_pos - 1), notch_prev);
  poly_data->GetCell(notch_cell_id)->GetPoints()->GetPoint(notch_pos, notch);
  poly_data->GetCell(notch_cell_id)->GetPoints()->GetPoint((notch_pos + 1) % n_points_cell, notch_next);

  // Vector between p0 and p1
  double v1[3] = {notch[0] - notch_prev[0], notch[1] - notch_prev[1], notch[2] - notch_prev[2]};
  // Vector between p2 and p1. Vector reference
  double v2[3] = {notch[0] - notch_next[0], notch[1] - notch_next[1], notch[2] - notch_next[2]};
  vtkMath::Normalize(v1);
  vtkMath::Normalize(v2);
  double bisector[3] = {v1[0] + v2[0], v1[1] + v2[1], v1[2] + v2[2]}; // Vector in the bisector direction
  vtkMath::Normalize(bisector);
  double bounds[6];
  poly_data->GetPoints()->GetBounds(bounds);
  double bound_min[3] = {bounds[0], bounds[2], bounds[4]};
  double bound_max[3] = {bounds[1], bounds[3], bounds[5]};

  // Maximum distance between all points in a polyData
  double d_max = sqrt(vtkMath::Distance2BetweenPoints(bound_min, bound_max));

  // Bisector: between notch and bisector_p. bisector_p = notch + d_max*bisector
  double bisector_p[3];
  for (unsigned i(0); i < 3; ++i)
    bisector_p[i] = notch[i] + d_max * bisector[i];

  // find the intersection point
  double vertex_coordinate = DBL_MAX;
  bool find_intersection = false; // flag
  vtkIdType n_cells = poly_data->GetNumberOfCells();

  for (vtkIdType cell_id(0); cell_id < n_cells; ++cell_id) // Verify intersections with the geometries
  {
    vtkIdType n_edges = poly_data->GetCell(cell_id)->GetNumberOfEdges();
    for (vtkIdType edge_id(0); edge_id < n_edges; ++edge_id)
    {
      double e_p1[3];
      double e_p2[3];
      poly_data->GetCell(cell_id)->GetEdge(edge_id)->GetPoints()->GetPoint(0, e_p1);
      poly_data->GetCell(cell_id)->GetEdge(edge_id)->GetPoints()->GetPoint(1, e_p2);
      double u;
      double v;
      int intersection = 0;
      intersection = vtkLine::Intersection3D(notch, bisector_p, e_p1, e_p2, u, v);
      if (u < vertex_coordinate && intersection == 2 && std::abs(u) > calculation_tol_
          && std::abs(u - 1) > calculation_tol_) // u!= 0 &&0u!=1
      {
        //save point
        find_intersection = true;
        vertex_coordinate = u;
        vertex_cell_id = cell_id;
        vertex_pos = edge_id;
      }
    }
  }

  if (find_intersection == false)
  {
    ROS_ERROR_STREAM("findIntersectWithBisector: Intersection not found");
    return false;
  }

  for (unsigned i = 0; i < 3; i++)
    vertex[i] = notch[i] + vertex_coordinate * d_max * bisector[i];

  return true;
}

bool DonghongDing::divideInConvexPolygons(PolygonVector &polygon_source,
                                          const int polygon_position,
                                          const vtkSmartPointer<vtkPoints> split_points)
{
  removeDuplicatePoints(polygon_source[polygon_position]);
  mergeColinearEdges(polygon_source[polygon_position]);

  vtkSmartPointer<vtkPolyData> poly_data = vtkSmartPointer<vtkPolyData>::New();
  poly_data->DeepCopy(polygon_source[polygon_position]);
  vtkIdType n_cells = poly_data->GetNumberOfCells();
  if (n_cells == 0)
  {
    ROS_WARN_STREAM("divideInConvexPolygons: poly_data is empty");
    return true;
  }
  if (n_cells == 1 && vtkPolygon::IsConvex(poly_data->GetPoints()))
  {
    return true;
  }

  //1. find first notch
  vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
  points = poly_data->GetPoints();

  vtkIdType notch_cell_id;
  vtkIdType notch_id;
  double angle;
  if (!findNotch(poly_data, notch_cell_id, notch_id, angle))
    return false;
  double notch[3];
  poly_data->GetCell(notch_cell_id)->GetPoints()->GetPoint(notch_id, notch);

  vtkIdType vertex_cell_id;
  vtkIdType vertex_pos; //position in the polygon
  vtkIdType vertex_edge_id; //edge id if this point is not a vertex --
  double vertex[3];

  //2. find second point of the dividing line (vertex)
  bool is_vertex(findVertex(poly_data, notch_cell_id, notch_id, vertex_cell_id, vertex_pos, angle));

  if (is_vertex)
    is_vertex = verifyAngles(poly_data, notch_cell_id, notch_id, vertex_cell_id, vertex_pos);
  //2.3. Eliminate notch
  if (is_vertex == false)
  {
    if (!findIntersectWithBisector(poly_data, notch_cell_id, notch_id, vertex_cell_id, vertex_edge_id, vertex))
      return false;
  }
  else
    poly_data->GetCell(vertex_cell_id)->GetPoints()->GetPoint(vertex_pos, vertex);
  //3. make the news polydatas (poly_data_a, poly_data_b)

  //    4 cases:
  //    case 1: notch and vertex are inside the same contour (polygon 0)
  //    case 2: notch is in the external contour and vertex is in a contour child
  //    case 3: notch is in a contour child and vertex is in the external contour
  //    case 4: notch and vertex are in different contour children
  // poly_data_a and poly_data_b
  vtkSmartPointer<vtkCellArray> polygon_array_a = vtkSmartPointer<vtkCellArray>::New();
  vtkSmartPointer<vtkPoints> points_a = vtkSmartPointer<vtkPoints>::New();

  vtkSmartPointer<vtkCellArray> polygon_array_b = vtkSmartPointer<vtkCellArray>::New();
  vtkSmartPointer<vtkPoints> points_b = vtkSmartPointer<vtkPoints>::New();

  vtkSmartPointer<vtkPoints> point_list = vtkSmartPointer<vtkPoints>::New();
  vtkSmartPointer<vtkPolygon> polygon_a = vtkSmartPointer<vtkPolygon>::New();
  vtkSmartPointer<vtkPolygon> polygon_b = vtkSmartPointer<vtkPolygon>::New();

  double p[3];
  // CASE 1,
  //    -vertex_pos > notch_id (case 1.1)
  //    -vertex_pos < notch_id (case 1.2)

  // save the external contours
  point_list = poly_data->GetCell(0)->GetPoints();
  vtkIdType last_point_a;
  vtkIdType first_point_b;
  if (notch_cell_id == 0 && vertex_cell_id == 0)
  {
    if (is_vertex == true)
    {
      split_points->InsertNextPoint(notch);
      split_points->InsertNextPoint(vertex);
      last_point_a = vertex_pos;
      first_point_b = vertex_pos;
    }
    else
    {
      split_points->InsertNextPoint(vertex);
      split_points->InsertNextPoint(notch);
      last_point_a = vertex_edge_id;
      first_point_b = (vertex_edge_id + 1) % point_list->GetNumberOfPoints(); // if Vertex is between the last point and the first point
          // Save Vertex in poly_data_a
      polygon_a->GetPointIds()->InsertNextId(points_a->GetNumberOfPoints());
      polygon_a->GetPoints()->InsertNextPoint(vertex);
      points_a->InsertNextPoint(vertex);
      // Save Vertex in poly_data_b
      polygon_b->GetPointIds()->InsertNextId(points_b->GetNumberOfPoints());
      polygon_b->GetPoints()->InsertNextPoint(vertex);
      points_b->InsertNextPoint(vertex);
    }
    vtkIdType n_points = point_list->GetNumberOfPoints();
    for (vtkIdType i(0); i < n_points; ++i)
    {
      int id = (i + notch_id) % n_points;
      point_list->GetPoint(id, p);
      if (first_point_b > notch_id)
      {
        if (id >= notch_id && id <= last_point_a)
        {
          polygon_a->GetPointIds()->InsertNextId(points_a->GetNumberOfPoints());
          polygon_a->GetPoints()->InsertNextPoint(p);
          points_a->InsertNextPoint(p);
        }
        if (id >= first_point_b || id < notch_id)
        {
          polygon_b->GetPointIds()->InsertNextId(points_b->GetNumberOfPoints());
          polygon_b->GetPoints()->InsertNextPoint(p);
          points_b->InsertNextPoint(p);
        }
      }
      else if (first_point_b < notch_id)
      {
        if (id >= notch_id || (last_point_a < notch_id && id <= last_point_a))
        {
          polygon_a->GetPointIds()->InsertNextId(points_a->GetNumberOfPoints());
          polygon_a->GetPoints()->InsertNextPoint(p);
          points_a->InsertNextPoint(p);
        }

        if (id >= first_point_b && id < notch_id)
        {
          polygon_b->GetPointIds()->InsertNextId(points_b->GetNumberOfPoints());
          polygon_b->GetPoints()->InsertNextPoint(p);
          points_b->InsertNextPoint(p);
        }
      }
    }
    point_list->GetPoint(notch_id, p);
    polygon_b->GetPointIds()->InsertNextId(points_b->GetNumberOfPoints());
    polygon_b->GetPoints()->InsertNextPoint(p);
    points_b->InsertNextPoint(p);

    polygon_array_a->InsertNextCell(polygon_a);
    polygon_array_b->InsertNextCell(polygon_b);
    //save the others polygons in the polydata
    vtkSmartPointer<vtkPolygon> external_contour_a = vtkSmartPointer<vtkPolygon>::New();
    external_contour_a->DeepCopy(polygon_a);
    for (vtkIdType i = 1; i < n_cells; i++)
    {
      polygon_a = vtkSmartPointer<vtkPolygon>::New();
      polygon_b = vtkSmartPointer<vtkPolygon>::New();
      point_list = poly_data->GetCell(i)->GetPoints();
      point_list->GetPoint(0, p);
      double n[3];
      computeNormal(external_contour_a->GetPoints(), n);
      double bounds[6];
      external_contour_a->GetPoints()->GetBounds(bounds);

      int in_polygon_a = external_contour_a->PointInPolygon(
          p, external_contour_a->GetPoints()->GetNumberOfPoints(),
          static_cast<double*>(external_contour_a->GetPoints()->GetData()->GetVoidPointer(0)),
          bounds, n);

      if (in_polygon_a == 1) // Save the contour in poly_data_a
      {
        for (vtkIdType j = 0; j < point_list->GetNumberOfPoints(); ++j)
        {
          point_list->GetPoint(j, p);
          polygon_a->GetPointIds()->InsertNextId(points_a->GetNumberOfPoints());
          polygon_a->GetPoints()->InsertNextPoint(p);
          points_a->InsertNextPoint(p);
        }
        polygon_array_a->InsertNextCell(polygon_a);
      }
      if (in_polygon_a == 0) // Save the contour in poly_data_b
      {
        for (vtkIdType j = 0; j < point_list->GetNumberOfPoints(); ++j)
        {
          point_list->GetPoint(j, p);
          polygon_b->GetPointIds()->InsertNextId(points_b->GetNumberOfPoints());
          polygon_b->GetPoints()->InsertNextPoint(p);
          points_b->InsertNextPoint(p);
        }
        polygon_array_b->InsertNextCell(polygon_b);
      }
    }
  }
  // cases 2,3 and 4
  vtkIdType n_points;
  if (notch_cell_id != vertex_cell_id) // 1 polydata (polydata_a)
  {
    vtkIdType first_point_vertex_contour;
    if (notch_cell_id != 0 && vertex_cell_id != 0) //CASE 4
    {
      //External contour in case 4
      polygon_a = vtkSmartPointer<vtkPolygon>::New();
      point_list = poly_data->GetCell(0)->GetPoints();
      for (vtkIdType j = 0; j < point_list->GetNumberOfPoints(); ++j)
      {
        point_list->GetPoint(j, p);
        polygon_a->GetPointIds()->InsertNextId(points_a->GetNumberOfPoints());
        points_a->InsertNextPoint(p);
      }
      polygon_array_a->InsertNextCell(polygon_a);
      polygon_a = vtkSmartPointer<vtkPolygon>::New();
    }
    // notch contour
    point_list = poly_data->GetCell(notch_cell_id)->GetPoints();
    n_points = point_list->GetNumberOfPoints();
    double delta[3];

    for (vtkIdType i = 0; i <= n_points; i++)
    {
      vtkIdType pos = (i + notch_id) % n_points;
      point_list->GetPoint(pos, p);
      //modify the notch point
      if (i == 0)
      {
        double next_point[3];
        point_list->GetPoint((i + 1 + notch_id) % n_points, next_point);
        vtkMath::Subtract(next_point, p, delta);
        vtkMath::Normalize(delta);
        vtkMath::MultiplyScalar(delta, 1e-7);
        vtkMath::Add(p, delta, p);
      }
      polygon_a->GetPointIds()->InsertNextId(points_a->GetNumberOfPoints());
      polygon_a->GetPoints()->InsertNextPoint(p);
      points_a->InsertNextPoint(p);
    }
    //vertex contour
    point_list = poly_data->GetCell(vertex_cell_id)->GetPoints();
    n_points = point_list->GetNumberOfPoints();
    if (is_vertex == true)
      first_point_vertex_contour = vertex_pos;
    else
    {
      first_point_vertex_contour = (vertex_edge_id + 1) % n_points;
      polygon_a->GetPointIds()->InsertNextId(points_a->GetNumberOfPoints());
      points_a->InsertNextPoint(vertex);
    }

    for (vtkIdType i = 0; i < n_points; i++)
    {
      vtkIdType pos = (i + first_point_vertex_contour) % n_points;
      point_list->GetPoint(pos, p);
      polygon_a->GetPointIds()->InsertNextId(points_a->GetNumberOfPoints());
      points_a->InsertNextPoint(p);
    }
    polygon_a->GetPointIds()->InsertNextId(points_a->GetNumberOfPoints());
    vtkMath::Add(vertex, delta, vertex);
    points_a->InsertNextPoint(vertex);

    polygon_array_a->InsertNextCell(polygon_a);
    //save the others polygons in the polydata
    for (vtkIdType i = 1; i < n_cells; i++)
    {
      if (i != vertex_cell_id && i != notch_cell_id)
      {
        polygon_a = vtkSmartPointer<vtkPolygon>::New();
        point_list = poly_data->GetCell(i)->GetPoints();
        for (vtkIdType j = 0; j < point_list->GetNumberOfPoints(); ++j)
        {
          point_list->GetPoint(j, p);
          polygon_a->GetPointIds()->InsertNextId(points_a->GetNumberOfPoints());
          points_a->InsertNextPoint(p);
        }
        polygon_array_a->InsertNextCell(polygon_a);
      }
    }
  }
// end cases
  vtkSmartPointer<vtkPolyData> poly_data_a = vtkSmartPointer<vtkPolyData>::New();
  poly_data_a->SetPolys(polygon_array_a);
  poly_data_a->SetPoints(points_a);
  polygon_source.erase(polygon_source.begin() + polygon_position);
  polygon_source.push_back(poly_data_a);

  if (polygon_array_b->GetNumberOfCells() > 0)
  {
    vtkSmartPointer<vtkPolyData> poly_data_b = vtkSmartPointer<vtkPolyData>::New();
    poly_data_b->SetPolys(polygon_array_b);
    poly_data_b->SetPoints(points_b);
    polygon_source.push_back(poly_data_b);
  }
  return true;
}

bool DonghongDing::removeDuplicatePoints(const Polygon poly_data,
                                         const double tolerance)
{/*
 vtkSmartPointer<vtkCellArray> polygon_array = vtkSmartPointer<vtkCellArray>::New();
 vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
 vtkSmartPointer<vtkPolyData> new_poly_data = vtkSmartPointer<vtkPolyData>::New();
 vtkIdType n_cells = poly_data->GetNumberOfCells();
 for (vtkIdType cell_id(0); cell_id < n_cells; ++cell_id)
 {
 vtkIdType n_points = poly_data->GetCell(cell_id)->GetNumberOfPoints();
 vtkSmartPointer<vtkPolygon> polygon = vtkSmartPointer<vtkPolygon>::New();
 for (vtkIdType point_id(0); point_id < n_points; ++point_id)
 {
 double p0[3];
 double p1[3];
 poly_data->GetCell(cell_id)->GetPoints()->GetPoint(point_id, p0);
 poly_data->GetCell(cell_id)->GetPoints()->GetPoint((point_id + 1) % n_points, p1);
 // 1. duplicate point
 if (vtkMath::Distance2BetweenPoints(p0, p1) > tolerance)
 {
 polygon->GetPointIds()->InsertNextId(points->GetNumberOfPoints());
 points->InsertNextPoint(p0);
 }
 }
 polygon_array->InsertNextCell(polygon);
 }
 new_poly_data->SetPolys(polygon_array);
 new_poly_data->SetPoints(points);

 poly_data->ShallowCopy(new_poly_data);
 */

  vtkSmartPointer<vtkCleanPolyData> cleaner = vtkSmartPointer<vtkCleanPolyData>::New();
  cleaner->SetInputData(poly_data);
  cleaner->ToleranceIsAbsoluteOn();
  cleaner->SetAbsoluteTolerance(tolerance);
  cleaner->Update();
  if (cleaner->GetOutput()->GetNumberOfPoints() < 3)
  {
    ROS_ERROR_STREAM(
                     "removeDuplicatePoints: Not enough points" << std::endl <<
                     "vtkPolyData contains "<< cleaner->GetOutput()->GetNumberOfPoints() << " points");
    return false;
  }
  poly_data->ShallowCopy(cleaner->GetOutput());
  return true;

}

bool DonghongDing::mergeColinearEdges(const Polygon polygon,
                                      const double tolerance)
{
  vtkSmartPointer<vtkCellArray> polygon_array = vtkSmartPointer<vtkCellArray>::New();
  vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
  vtkSmartPointer<vtkPolyData> new_poly_data = vtkSmartPointer<vtkPolyData>::New();

  vtkIdType n_cells = polygon->GetNumberOfCells();
  for (vtkIdType cell_id(0); cell_id < n_cells; ++cell_id)
  {
    vtkIdType n_points = polygon->GetCell(cell_id)->GetNumberOfPoints();
    vtkSmartPointer<vtkPolygon> contour = vtkSmartPointer<vtkPolygon>::New();
    double p0[3];
    polygon->GetCell(cell_id)->GetPoints()->GetPoint(0, p0); //fist point in this contour
    for (vtkIdType point_id(0); point_id < n_points; ++point_id)
    {
      double p1[3]; // Central point
      double p2[3];
      polygon->GetCell(cell_id)->GetPoints()->GetPoint((point_id + 1) % n_points, p1);
      polygon->GetCell(cell_id)->GetPoints()->GetPoint((point_id + 2) % n_points, p2);

      double distance = sqrt(vtkLine::DistanceToLine(p1, p0, p2));
      if (std::abs(distance) > tolerance)
      {

        contour->GetPointIds()->InsertNextId(points->GetNumberOfPoints());
        points->InsertNextPoint(p1);
        p0[0] = p1[0];
        p0[1] = p1[1];
        p0[2] = p1[2];
      }

    }
    if (contour->GetPointIds()->GetNumberOfIds() < 3)
    {
      ROS_ERROR_STREAM(
          "mergeColinearEdges: Cell " << cell_id << " must have at least 3 points outside the region of tolerance");

      return false;
    }
    polygon_array->InsertNextCell(contour);
  }

  new_poly_data->SetPolys(polygon_array);
  new_poly_data->SetPoints(points);

  polygon->ShallowCopy(new_poly_data);
  return true;
}

bool DonghongDing::offsetPolygonContour(const Polygon poly_data,
                                        const double deposited_material_width)
{
  vtkSmartPointer<vtkCellArray> polygon_array = vtkSmartPointer<vtkCellArray>::New();
  vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
  vtkIdType n_cells = poly_data->GetNumberOfCells();
  for (vtkIdType cell_id = 0; cell_id < n_cells; ++cell_id)
  {
    vtkIdType n_points = poly_data->GetCell(cell_id)->GetNumberOfPoints();
    vtkSmartPointer<vtkPolygon> polygon = vtkSmartPointer<vtkPolygon>::New();
    for (vtkIdType point_id = 0; point_id < n_points; ++point_id)
    {
      double p0[3];
      double p1[3]; // Central point
      double p2[3];
      if (point_id == 0)
        poly_data->GetCell(cell_id)->GetPoints()->GetPoint((n_points - 1), p0);
      else
        poly_data->GetCell(cell_id)->GetPoints()->GetPoint((point_id - 1), p0);
      poly_data->GetCell(cell_id)->GetPoints()->GetPoint(point_id, p1);
      poly_data->GetCell(cell_id)->GetPoints()->GetPoint((point_id + 1) % n_points, p2);
      double v2[3];
      double v1[3];
      vtkMath::Subtract(p0, p1, v1);
      vtkMath::Subtract(p2, p1, v2);
      vtkMath::Normalize(v1);
      vtkMath::Normalize(v2);
      double angle = vtkMath::AngleBetweenVectors(v1, v2);
      double bisector[3];
      vtkMath::Add(v1, v2, bisector);
      vtkMath::Normalize(bisector);
      double new_point[3];
      for (int k = 0; k < 3; ++k)
        new_point[k] = p1[k] + deposited_material_width / sin(angle / 2) * bisector[k];

      polygon->GetPointIds()->InsertNextId(points->GetNumberOfPoints());
      polygon->GetPoints()->InsertNextPoint(new_point);
      points->InsertNextPoint(new_point);
    }

    // First verification: intersection among the lines
    for (vtkIdType i(0); i < n_points; ++i)
    {
      double p_i[3];
      double new_p_i[3];
      poly_data->GetCell(cell_id)->GetPoints()->GetPoint(i, p_i);
      polygon->GetPoints()->GetPoint(i, new_p_i);

      for (vtkIdType j(0); j < n_points; ++j)
      {
        double p_j[3];
        double new_p_j[3];
        poly_data->GetCell(cell_id)->GetPoints()->GetPoint(j, p_j);
        polygon->GetPoints()->GetPoint(j, new_p_j);

        double u; // Parametric coordinate of the line 1
        double v; // Parametric coordinate of the line 2
        int intersection = vtkLine::Intersection3D(p_i, new_p_i, p_j, new_p_j, u, v);

        if (intersection == 2)
        {
          ROS_ERROR_STREAM("offsetPolygonContour: one or multiple edges are too short! Lines: " << i << " and " << j);
          return false;
        }
      }
    }
    polygon_array->InsertNextCell(polygon);
  }
  poly_data->SetPolys(polygon_array);
  poly_data->SetPoints(points);

  return true;
}

double DonghongDing::identifyZigzagDirection(const Polygon poly_data,
                                             vtkIdType &edge,
                                             vtkIdType &opposite_point_id)
{
  if (poly_data->GetNumberOfCells() != 1)
  {
    ROS_ERROR_STREAM("identifyZigzagDirection: multiple or zero cells in the polydata");
    return -1;
  }

  vtkIdType n_points = poly_data->GetCell(0)->GetNumberOfPoints(); // number of points is equal to number of edges
  double min_global_h = DBL_MAX;

  for (vtkIdType edge_id(0); edge_id < n_points; ++edge_id)
  {
    double e_p1[3];
    double e_p2[3];
    poly_data->GetCell(0)->GetEdge(edge_id)->GetPoints()->GetPoint(0, e_p1);
    poly_data->GetCell(0)->GetEdge(edge_id)->GetPoints()->GetPoint(1, e_p2);

    double max_local_h = 0;
    vtkIdType local_opposite_point = 0; // opposite point to the edge (edge_id)
    for (vtkIdType point_id(0); point_id < n_points; ++point_id)
    {
      if (point_id != edge_id && point_id != (edge_id + 1) % n_points)
      {
        double point[3];
        poly_data->GetCell(0)->GetPoints()->GetPoint(point_id, point);
        double h = sqrt(vtkLine::DistanceToLine(point, e_p1, e_p2));

        double delta_h = h - max_local_h;
        if (delta_h > calculation_tol_) // h > max_local_h
        {
          max_local_h = h;
          local_opposite_point = point_id;
        }
        else if (std::abs(delta_h) < calculation_tol_) // two points at the same distance. Find the closest point to e_p2
        {
          double max_point[3];
          poly_data->GetCell(0)->GetPoints()->GetPoint(local_opposite_point, max_point);

          if (vtkMath::Distance2BetweenPoints(point, e_p2) < vtkMath::Distance2BetweenPoints(max_point, e_p2))
            local_opposite_point = point_id;
        }
      }
    }
    if (max_local_h < min_global_h)
    {
      min_global_h = max_local_h;
      edge = edge_id;
      opposite_point_id = local_opposite_point;
    }
  }
  return min_global_h;
}

void DonghongDing::identifyLeftChain(const Polygon poly_data,
                                     const vtkIdType edge_id,
                                     const vtkIdType opposite_point_id,
                                     const vtkSmartPointer<vtkPoints> left_chain,
                                     const vtkSmartPointer<vtkPoints> right_chain)
{
  vtkIdType n_points = poly_data->GetCell(0)->GetNumberOfPoints();
  // edge points :
  double e_p1[3];
  double e_p2[3];
  poly_data->GetCell(0)->GetEdge(edge_id)->GetPoints()->GetPoint(0, e_p1);
  poly_data->GetCell(0)->GetEdge(edge_id)->GetPoints()->GetPoint(1, e_p2);

  double p[3];
  poly_data->GetCell(0)->GetPoints()->GetPoint(opposite_point_id, p);
  vtkIdType last_point_id = opposite_point_id;
  vtkIdType first_point_id = (edge_id + 1) % n_points;

  poly_data->GetCell(0)->GetPoints()->GetPoint(last_point_id, p);
  right_chain->InsertNextPoint(p);

  for (vtkIdType i(0); i < n_points; ++i)
  {
    int id = (i + first_point_id) % n_points;

    poly_data->GetCell(0)->GetPoints()->GetPoint(id, p);

    if (last_point_id > first_point_id)
    {
      if (id >= first_point_id && id <= last_point_id)
        left_chain->InsertNextPoint(p);
      else
        right_chain->InsertNextPoint(p);
    }
    else
    {
      if (id < first_point_id && id > last_point_id)
        right_chain->InsertNextPoint(p);
      else
        left_chain->InsertNextPoint(p);
    }
  }
}

bool DonghongDing::offsetLeftChain(const Polygon poly_data,
                                   const vtkIdType edge_id,
                                   const vtkIdType opposite_point_id,
                                   const vtkSmartPointer<vtkPoints> left_chain,
                                   const vtkSmartPointer<vtkPoints> right_chain)
{
  if (poly_data->GetNumberOfCells() != 1)
  {
    ROS_ERROR_STREAM("offsetLeftChain: multiple or zero cells in the polydata");
    return false;
  }
  vtkIdType n_points = poly_data->GetCell(0)->GetNumberOfPoints();
  double e_p1[3];
  double e_p2[3];
  poly_data->GetCell(0)->GetEdge(edge_id)->GetPoints()->GetPoint(0, e_p1);
  poly_data->GetCell(0)->GetEdge(edge_id)->GetPoints()->GetPoint(1, e_p2);

  vtkIdType first_point_id = (edge_id + 1) % n_points;
  // 1. offset the polygon
  // 1.1. identify the new points
  double angle_first_point;
  vtkSmartPointer<vtkPoints> new_points = vtkSmartPointer<vtkPoints>::New();
  for (vtkIdType i(0); i < left_chain->GetNumberOfPoints(); ++i)
  {
    vtkIdType id = (first_point_id + i) % n_points;
    double p0[3];
    double p1[3]; //point
    double p2[3];

    double v1[3];
    double v2[3];
    double angle;
    double bisector[3];
    double new_point[3];

    if (id == 0)
      poly_data->GetCell(0)->GetPoints()->GetPoint((n_points - 1), p0);
    else
      poly_data->GetCell(0)->GetPoints()->GetPoint((id - 1), p0);
    poly_data->GetCell(0)->GetPoints()->GetPoint(id, p1);
    poly_data->GetCell(0)->GetPoints()->GetPoint((id + 1) % n_points, p2);

    vtkMath::Subtract(p0, p1, v1);
    vtkMath::Subtract(p2, p1, v2);
    vtkMath::Normalize(v1);
    vtkMath::Normalize(v2);
    angle = vtkMath::AngleBetweenVectors(v1, v2);
    if (i == 0)
    {
      angle_first_point = angle; // Save the value to modify the last point in the first hull
      for (int k = 0; k < 3; ++k)
        new_point[k] = p1[k] + deposited_material_width_ / sin(angle) * v1[k];
      if (sqrt(vtkMath::Distance2BetweenPoints(p1, new_point)) > sqrt(vtkMath::Distance2BetweenPoints(p1, p0)))
      {
        ROS_ERROR_STREAM("offsetLeftChain: error modifying the first point in the left chain");
        return false;
      }
    }
    else if (i == (left_chain->GetNumberOfPoints() - 1))
    {
      for (int k = 0; k < 3; ++k)
        new_point[k] = p1[k] + deposited_material_width_ / sin(angle) * v2[k];
      if (sqrt(vtkMath::Distance2BetweenPoints(p1, new_point)) > sqrt(vtkMath::Distance2BetweenPoints(p1, p2)))
      {
        ROS_ERROR_STREAM("offsetLeftChain: error modifying the last point in the left chain");
        return false;
      }
    }
    else
    {
      vtkMath::Add(v1, v2, bisector);
      vtkMath::Normalize(bisector);
      for (int k = 0; k < 3; ++k)
        new_point[k] = p1[k] + deposited_material_width_ / sin(angle / 2) * bisector[k];
    }
    new_points->InsertNextPoint(new_point);
  }

  // 1.2. verification, intersection among the lines
  for (vtkIdType i(0); i < left_chain->GetNumberOfPoints(); ++i)
  {
    double p_i[3];
    double new_p_i[3];
    left_chain->GetPoint(i, p_i);
    new_points->GetPoint(i, new_p_i);

    for (vtkIdType j(0); j < left_chain->GetNumberOfPoints(); ++j)
    {
      double p_j[3];
      double new_p_j[3];
      left_chain->GetPoint(i, p_j);
      new_points->GetPoint(i, new_p_j);

      double u; //  Parametric coordinate of the line 1
      double v; //  Parametric coordinate of the line 2
      int intersection = vtkLine::Intersection3D(p_i, new_p_i, p_j, new_p_j, u, v);

      if (intersection == 2)
      {
        ROS_ERROR_STREAM("offsetLeftChain: edge is too short");
        return false;
      }
    }
  }

  // 1.3. replace the points in the poly_data
  for (vtkIdType i(0); i < left_chain->GetNumberOfPoints(); ++i)
  {
    vtkIdType id = (first_point_id + i) % n_points;
    double new_point[3];
    new_points->GetPoint(i, new_point);
    poly_data->GetPoints()->SetPoint(poly_data->GetCell(0)->GetPointId(id), new_point);
    poly_data->Modified();
  }

  // 2. modify  first point in the second hull
  double p0[3]; // first point in the second hull
  double p1[3]; // opposite point in the polygon after offset
  double p2[3]; // next point to p1
  double v[3];
  right_chain->GetPoint(0, p0);
  poly_data->GetCell(0)->GetPoints()->GetPoint(opposite_point_id, p1);
  poly_data->GetCell(0)->GetPoints()->GetPoint((opposite_point_id + 1) % n_points, p2);

  if (vtkMath::Distance2BetweenPoints(p0, p1) >= vtkMath::Distance2BetweenPoints(p1, p2))
  {
    ROS_ERROR_STREAM("offsetLeftChain: Error modifying the point between the zigzag path and the right chain");
    return false;
  }

  vtkMath::Subtract(p1, p0, v);
  for (int k = 0; k < 3; ++k)
    p0[k] += 2 * v[k];
  right_chain->SetPoint(0, p0);

  // 3. add modified points in the first hull
  for (vtkIdType i(left_chain->GetNumberOfPoints() - 1); i >= 0; --i)
  {
    vtkIdType id = (first_point_id + i) % n_points;
    double p[3];
    poly_data->GetCell(0)->GetPoints()->GetPoint(id, p);
    left_chain->InsertNextPoint(p);
  }

  // 4. modify last point in the first hull
  left_chain->GetPoint((left_chain->GetNumberOfPoints() - 1), p0);
  left_chain->GetPoint((left_chain->GetNumberOfPoints() - 2), p1);
  vtkMath::Subtract(p1, p0, v);
  vtkMath::Normalize(v);
  if (sqrt(vtkMath::Distance2BetweenPoints(p0, p1)) < deposited_material_width_ / (sin(angle_first_point)))
  {
    ROS_ERROR_STREAM("offsetLeftChain: Error modifying the point between the left chain and the zigzag path");
    return false;
  }
  for (int k = 0; k < 3; ++k)
    p0[k] += deposited_material_width_ / (sin(angle_first_point)) * v[k];
  left_chain->SetPoint((left_chain->GetNumberOfPoints() - 1), p0);

  return true;
}

bool DonghongDing::zigzagGeneration(const Polygon poly_data,
                                    const vtkIdType edge_id,
                                    const vtkIdType opposite_point_id,
                                    const vtkSmartPointer<vtkPoints> zigzag_points,
                                    const double deposited_material_width)
{
  if (poly_data->GetNumberOfCells() != 1)
  {
    ROS_ERROR_STREAM("zigzagGeneration: multiple or zero cells in the polydata");
    return false;
  }
  vtkIdType n_edges = poly_data->GetCell(0)->GetNumberOfEdges();
  double e_p1[3];
  double e_p2[3];
  double last_p[3];
  double next_p[3];
  poly_data->GetCell(0)->GetEdge(edge_id)->GetPoints()->GetPoint(0, e_p1);
  poly_data->GetCell(0)->GetEdge(edge_id)->GetPoints()->GetPoint(1, e_p2);
  poly_data->GetPoints()->GetPoint(opposite_point_id, last_p);
  poly_data->GetPoints()->GetPoint((opposite_point_id + 1) % n_edges, next_p);
  double zigzag_dir[3]; // zigzag direction vector

  vtkMath::Subtract(e_p1, e_p2, zigzag_dir);
  vtkMath::Normalize(zigzag_dir);

  double u[3]; //vector between the first point in the edge with the zigzag direction and the opposite point
  vtkMath::Subtract(last_p, e_p2, u);
  double proj_u[3];
  vtkMath::ProjectVector(u, zigzag_dir, proj_u);
  double step_dir[3]; // step direction vector. Is perpendicular to zigzag_dir
  vtkMath::Subtract(u, proj_u, step_dir);
  double h = vtkMath::Norm(step_dir);
  vtkMath::Normalize(step_dir);

  double l = deposited_material_width;
  zigzag_points->InsertNextPoint(e_p2);
  zigzag_points->InsertNextPoint(e_p1);

  while (l <= h) // intersect the polygon with a perpendicular plane
  {

    vtkSmartPointer<vtkPoints> intersection_points = vtkSmartPointer<vtkPoints>::New();
    for (vtkIdType edge_id(0); edge_id < n_edges; ++edge_id)
    {
      double p1[3];
      double p2[3];
      poly_data->GetCell(0)->GetEdge(edge_id)->GetPoints()->GetPoint(0, p1);
      poly_data->GetCell(0)->GetEdge(edge_id)->GetPoints()->GetPoint(1, p2);
      double p0[3];
      for (unsigned k(0); k < 3; ++k)
        p0[k] = e_p2[k] + l * step_dir[k];
      double t;
      double x[3];
      int intersection = vtkPlane::IntersectWithLine(p1, p2, step_dir, p0, t, x);
      if (intersection == 1)
        intersection_points->InsertNextPoint(x);
    }

    if (intersection_points->GetNumberOfPoints() != 2)
    {
      ROS_ERROR_STREAM(
          "zigzagGeneration: " << intersection_points->GetNumberOfPoints() << " intersections. 2 expected intersections.");
      return false;
    }

    double final_point[3];
    double x_1[3];
    double x_2[3];
    zigzag_points->GetPoint((zigzag_points->GetNumberOfPoints() - 1), final_point);
    intersection_points->GetPoint(0, x_1);
    intersection_points->GetPoint(1, x_2);

    if (vtkMath::Distance2BetweenPoints(final_point, x_1) < vtkMath::Distance2BetweenPoints(final_point, x_2))
    {
      zigzag_points->InsertNextPoint(x_1);
      zigzag_points->InsertNextPoint(x_2);
    }
    else
    {
      zigzag_points->InsertNextPoint(x_2);
      zigzag_points->InsertNextPoint(x_1);
    }
    l += deposited_material_width;
  }

  // Opposite edges are parallel
  double v1[3] = {e_p1[0] - e_p2[0], e_p1[1] - e_p2[1], e_p1[2] - e_p2[2]};
  double v2[3] = {next_p[0] - last_p[0], next_p[1] - last_p[1], next_p[2] - last_p[2]};
  if (vtkMath::AngleBetweenVectors(v1, v2) < 1e-3) // Almost parallel
    zigzag_points->InsertNextPoint(next_p);
  zigzag_points->InsertNextPoint(last_p);
  return true;
}

void DonghongDing::mergeListOfPoints(const Polygon poly_data,
                                     const vtkSmartPointer<vtkPoints> left_chain,
                                     const vtkSmartPointer<vtkPoints> right_chain,
                                     const vtkSmartPointer<vtkPoints> zigzag_points)
{
  vtkSmartPointer<vtkPolygon> polygon = vtkSmartPointer<vtkPolygon>::New();
  vtkSmartPointer<vtkCellArray> polygon_array = vtkSmartPointer<vtkCellArray>::New();
  vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
  vtkSmartPointer<vtkPolyData> new_poly_data = vtkSmartPointer<vtkPolyData>::New();
  double p[3];
  for (vtkIdType i = 0; i < right_chain->GetNumberOfPoints(); ++i)
  {

    right_chain->GetPoint(i, p);
    polygon->GetPointIds()->InsertNextId(points->GetNumberOfPoints());
    points->InsertNextPoint(p);
  }
  for (vtkIdType i = 0; i < left_chain->GetNumberOfPoints(); ++i)
  {

    left_chain->GetPoint(i, p);
    polygon->GetPointIds()->InsertNextId(points->GetNumberOfPoints());
    points->InsertNextPoint(p);
  }
  for (vtkIdType i = 0; i < zigzag_points->GetNumberOfPoints(); ++i)
  {

    zigzag_points->GetPoint(i, p);
    polygon->GetPointIds()->InsertNextId(points->GetNumberOfPoints());
    points->InsertNextPoint(p);
  }

  polygon_array->InsertNextCell(polygon);
  new_poly_data->SetPolys(polygon_array);
  new_poly_data->SetPoints(points);

  poly_data->ShallowCopy(new_poly_data);
}

bool DonghongDing::mergeConvexPolygons(PolygonVector &polygon_source,
                                       const vtkSmartPointer<vtkPoints> split_points,
                                       const vtkIdType split_line)
{
  unsigned n_polygons = polygon_source.size();

  double l0[3];
  double l1[3];
  split_points->GetPoint((2 * split_line), l0);
  split_points->GetPoint((2 * split_line + 1), l1);

// Find the two closest polygons to the split line. the distance between the split line
  unsigned polygon_1 = -1;
  vtkIdType edge_1 = -1;
  double l0_to_edge_1 = DBL_MAX;

  unsigned polygon_2 = -1;
  vtkIdType edge_2 = -1;
  double l0_to_edge_2 = DBL_MAX;

  for (unsigned polygon_id(0); polygon_id < n_polygons; ++polygon_id)
  {
    bool find_parallel = false;
    double min_distance_to_l0 = DBL_MAX;
    vtkIdType closest_edge = -1;
    vtkIdType n_edges = polygon_source[polygon_id]->GetCell(0)->GetNumberOfEdges();
    for (int edge_id(0); edge_id < n_edges; ++edge_id)
    {
      double distance_to_l0;
      double e_p1[3];
      double e_p0[3];
      polygon_source[polygon_id]->GetCell(0)->GetEdge(edge_id)->GetPoints()->GetPoint(0, e_p0);
      polygon_source[polygon_id]->GetCell(0)->GetEdge(edge_id)->GetPoints()->GetPoint(1, e_p1);
      double t;

      double v[3] = {e_p1[0] - e_p0[0], e_p1[1] - e_p0[1], e_p1[2] - e_p0[2]};
      double u[3] = {l1[0] - l0[0], l1[1] - l0[1], l1[2] - l0[2]};
      if (sin(vtkMath::AngleBetweenVectors(u, v)) < 1e-3) // the lines are almost parallel
      {
        distance_to_l0 = sqrt(vtkLine::DistanceToLine(l0, e_p0, e_p1, t));
        if (distance_to_l0 < min_distance_to_l0)
        {
          find_parallel = true;
          min_distance_to_l0 = distance_to_l0;
          closest_edge = edge_id;
        }
      }
    }
    if (find_parallel)
    {
      if (min_distance_to_l0 < l0_to_edge_1)
      {
        polygon_2 = polygon_1;
        edge_2 = edge_1;
        l0_to_edge_2 = l0_to_edge_1;

        polygon_1 = polygon_id;
        edge_1 = closest_edge;
        l0_to_edge_1 = min_distance_to_l0;
      }
      else if (min_distance_to_l0 < l0_to_edge_2)
      {
        polygon_2 = polygon_id;
        edge_2 = closest_edge;
        l0_to_edge_2 = min_distance_to_l0;
      }
    }
  }
  if (edge_1 == -1 || edge_2 == -1)
  {
    ROS_ERROR_STREAM("mergeConvexPolygons: Error finding parallels edges to the split line");
    return false;
  }

  // Find the new points
  double e1_p1[3];
  double e1_p0[3];

  double e2_p1[3];
  double e2_p0[3];

  double e1_v[3];
  double e2_v[3];

  double connection_point_1[3];
  double connection_point_2[3];

  double connection_point_3[3];
  double connection_point_4[3];

  double t1 = -1;
  double t2 = -1;
  polygon_source[polygon_1]->GetCell(0)->GetEdge(edge_1)->GetPoints()->GetPoint(0, e1_p0);
  polygon_source[polygon_1]->GetCell(0)->GetEdge(edge_1)->GetPoints()->GetPoint(1, e1_p1);

  polygon_source[polygon_2]->GetCell(0)->GetEdge(edge_2)->GetPoints()->GetPoint(0, e2_p0);
  polygon_source[polygon_2]->GetCell(0)->GetEdge(edge_2)->GetPoints()->GetPoint(1, e2_p1);

  vtkMath::Subtract(e1_p1, e1_p0, e1_v);
  vtkMath::Normalize(e1_v);
  vtkMath::Subtract(e2_p1, e2_p0, e2_v);
  vtkMath::Normalize(e2_v);

  vtkLine::DistanceToLine(l0, e2_p0, e2_p1, t2, connection_point_2);
  t2 = (t2 > 1) ? 1 : (t2 < 0) ? 0 : t2;
  vtkLine::DistanceToLine(l0, e1_p0, e1_p1, t1, connection_point_1);
  t1 = (t1 > 1) ? 1 : (t1 < 0) ? 0 : t1;

  // Find the connection points. two points in each edge
  double l; // Distance between the connection lines
  double connection_v[3];

  vtkMath::Subtract(connection_point_2, connection_point_1, connection_v);
  vtkMath::Normalize(connection_v);
  l = deposited_material_width_ / sin(vtkMath::AngleBetweenVectors(e1_v, connection_v));
  double e1_l = sqrt(vtkMath::Distance2BetweenPoints(e1_p0, e1_p1));
  double e2_l = sqrt(vtkMath::Distance2BetweenPoints(e2_p0, e2_p1));

  if ((t2 - l / e2_l) > 0 && t1 < 1)
  {
    for (int k = 0; k < 3; ++k)
    {
      connection_point_4[k] = connection_point_1[k] + l * e1_v[k];
      connection_point_3[k] = connection_point_2[k] - l * e2_v[k];

      if (t1 == 0 && (l - e1_l) < 0.25 * deposited_material_width_ && (l - e1_l) > 0) // Arbitrary value 0.25 * d
        connection_point_4[k] = e1_p1[k];
    }
  }
  else if ((t1 - l / e1_l) > 0 && t2 < 1)
  {
    for (int k = 0; k < 3; ++k)
    {
      connection_point_4[k] = connection_point_1[k];
      connection_point_3[k] = connection_point_2[k];

      connection_point_1[k] -= l * e1_v[k];
      connection_point_2[k] += l * e2_v[k];

      if (t2 == 0 && (l - e2_l) < 0.25 * deposited_material_width_ && (l - e2_l) > 0) // Arbitrary value
        connection_point_2[k] = e2_p1[k];
    }
  }
  else
  {
    ROS_ERROR_STREAM("mergeConvexPolygons: edge is too short");
    return false;
  }

  // Merge polygons
  vtkSmartPointer<vtkCellArray> polygon_array = vtkSmartPointer<vtkCellArray>::New();
  vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
  vtkSmartPointer<vtkPolygon> polygon = vtkSmartPointer<vtkPolygon>::New();
  double p[3];

  for (vtkIdType i(0); i <= edge_1; ++i) // First polygon, part 1
  {
    polygon_source[polygon_1]->GetCell(0)->GetPoints()->GetPoint(i, p);
    polygon->GetPointIds()->InsertNextId(points->GetNumberOfPoints());
    points->InsertNextPoint(p);
  }

  polygon->GetPointIds()->InsertNextId(points->GetNumberOfPoints());
  points->InsertNextPoint(connection_point_1);

  polygon->GetPointIds()->InsertNextId(points->GetNumberOfPoints());
  points->InsertNextPoint(connection_point_2);

  for (vtkIdType i(0); i < polygon_source[polygon_2]->GetNumberOfPoints(); ++i) // Second polygon,
  {
    vtkIdType id = (i + edge_2 + 1) % polygon_source[polygon_2]->GetNumberOfPoints();
    polygon_source[polygon_2]->GetCell(0)->GetPoints()->GetPoint(id, p);
    polygon->GetPointIds()->InsertNextId(points->GetNumberOfPoints());
    points->InsertNextPoint(p);
  }

  polygon->GetPointIds()->InsertNextId(points->GetNumberOfPoints());
  points->InsertNextPoint(connection_point_3);

  polygon->GetPointIds()->InsertNextId(points->GetNumberOfPoints());
  points->InsertNextPoint(connection_point_4);

  for (vtkIdType i(edge_1 + 1); i < polygon_source[polygon_1]->GetNumberOfPoints(); ++i) // first polygon, part 2
  {
    polygon_source[polygon_1]->GetCell(0)->GetPoints()->GetPoint(i, p);
    polygon->GetPointIds()->InsertNextId(points->GetNumberOfPoints());
    points->InsertNextPoint(p);
  }

  polygon_array->InsertNextCell(polygon);
  vtkSmartPointer<vtkPolyData> poly_data = vtkSmartPointer<vtkPolyData>::New();
  poly_data->SetPolys(polygon_array);
  poly_data->SetPoints(points);

  if (polygon_2 > polygon_1)
  {
    polygon_source.erase(polygon_source.begin() + polygon_2);
    polygon_source.erase(polygon_source.begin() + polygon_1);
  }
  else
  {
    polygon_source.erase(polygon_source.begin() + polygon_1);
    polygon_source.erase(polygon_source.begin() + polygon_2);
  }

  polygon_source.push_back(poly_data);
  return true;
}

// VERSION 2 - Works but unsure if better
/*
 // Functions used in the second method to merge convex polygons
 bool DonghongDing::mergeConvexPolygons2(Polygons &polygon_source, vtkSmartPointer<vtkPoints> split_points, double d)
 {

 unsigned n_polygons = polygon_source.size();
 vtkIdType n_lines = split_points->GetNumberOfPoints() / 2;
 std::vector<Connection> connection_list;
 for (vtkIdType line_id(0); line_id < n_lines; ++line_id)
 {
 double l0[3];
 double l1[3];
 split_points->GetPoint((2 * line_id), l0);
 split_points->GetPoint((2 * line_id + 1), l1);
 Connection connection;
 connection.edge_id_1 = -1;
 connection.edge_id_2 = -1;
 connection.second_time = false;
 // connection.in_stack = false;
 double l0_to_edge_1 = DBL_MAX;
 double l0_to_edge_2 = DBL_MAX;

 for (unsigned polygon_id(0); polygon_id < n_polygons; ++polygon_id)
 {
 bool find_parallel = false;
 double min_distance_to_l0 = DBL_MAX;
 vtkIdType closest_edge = -1;
 vtkIdType n_edges = polygon_source[polygon_id]->GetCell(0)->GetNumberOfEdges();
 for (int edge_id(0); edge_id < n_edges; ++edge_id)
 {
 double distance_to_l0;
 double e_p1[3];
 double e_p0[3];
 polygon_source[polygon_id]->GetCell(0)->GetEdge(edge_id)->GetPoints()->GetPoint(0, e_p0);
 polygon_source[polygon_id]->GetCell(0)->GetEdge(edge_id)->GetPoints()->GetPoint(1, e_p1);
 double t;

 double v[3] = {e_p1[0] - e_p0[0], e_p1[1] - e_p0[1], e_p1[2] - e_p0[2]};
 double u[3] = {l1[0] - l0[0], l1[1] - l0[1], l1[2] - l0[2]};
 if (sin(vtkMath::AngleBetweenVectors(u, v)) < 1e-3) // the lines are almost parallel
 {
 distance_to_l0 = sqrt(vtkLine::DistanceToLine(l0, e_p0, e_p1, t));
 if (distance_to_l0 < min_distance_to_l0)
 {
 find_parallel = true;
 min_distance_to_l0 = distance_to_l0;
 closest_edge = edge_id;
 }
 }
 }
 if (find_parallel)
 {
 if (min_distance_to_l0 < l0_to_edge_1)
 {
 connection.polygon_id_2 = connection.polygon_id_1;
 connection.edge_id_2 = connection.edge_id_1;
 l0_to_edge_2 = l0_to_edge_1;

 connection.polygon_id_1 = polygon_id;
 connection.edge_id_1 = closest_edge;
 l0_to_edge_1 = min_distance_to_l0;
 }
 else if (min_distance_to_l0 < l0_to_edge_2)
 {
 connection.polygon_id_2 = polygon_id;
 connection.edge_id_2 = closest_edge;
 l0_to_edge_2 = min_distance_to_l0;
 }
 }
 }
 if (connection.edge_id_1 == -1 || connection.edge_id_2 == -1)
 {
 ROS_ERROR_STREAM("mergeConvexPolygons: Error finding parallels edges to the split line");
 return false;
 }
 // find the new points
 double e1_p1[3];
 double e1_p0[3];

 double e2_p1[3];
 double e2_p0[3];

 double e1_v[3];
 double e2_v[3];

 double t1 = -1;
 double t2 = -1;
 polygon_source[connection.polygon_id_1]->GetCell(0)->GetEdge(connection.edge_id_1)->GetPoints()->GetPoint(0, e1_p0);
 polygon_source[connection.polygon_id_1]->GetCell(0)->GetEdge(connection.edge_id_1)->GetPoints()->GetPoint(1, e1_p1);

 polygon_source[connection.polygon_id_2]->GetCell(0)->GetEdge(connection.edge_id_2)->GetPoints()->GetPoint(0, e2_p0);
 polygon_source[connection.polygon_id_2]->GetCell(0)->GetEdge(connection.edge_id_2)->GetPoints()->GetPoint(1, e2_p1);

 vtkMath::Subtract(e1_p1, e1_p0, e1_v);
 vtkMath::Normalize(e1_v);
 vtkMath::Subtract(e2_p1, e2_p0, e2_v);
 vtkMath::Normalize(e2_v);

 vtkLine::DistanceToLine(l0, e2_p0, e2_p1, t2, connection.p2);
 t2 = (t2 > 1) ? 1 : (t2 < 0) ? 0 : t2;
 vtkLine::DistanceToLine(l0, e1_p0, e1_p1, t1, connection.p1);
 t1 = (t1 > 1) ? 1 : (t1 < 0) ? 0 : t1;

 // Find the connection points. two points in each edge
 double l; // Distance between the connection lines
 double connection_v[3];

 vtkMath::Subtract(connection.p2, connection.p1, connection_v);
 vtkMath::Normalize(connection_v);
 l = d / sin(vtkMath::AngleBetweenVectors(e1_v, connection_v));
 double e1_l = sqrt(vtkMath::Distance2BetweenPoints(e1_p0, e1_p1));
 double e2_l = sqrt(vtkMath::Distance2BetweenPoints(e2_p0, e2_p1));

 if ((t2 - l / e2_l) > 0 && t1 < 1)
 {
 for (int k = 0; k < 3; ++k)
 {
 connection.p4[k] = connection.p1[k] + l * e1_v[k];
 connection.p3[k] = connection.p2[k] - l * e2_v[k];

 if (t1 == 0 && (l - e1_l) < 0.25 * d && (l - e1_l) > 0) // arbitrary value 0.25 * d
 connection.p4[k] = e1_p1[k];
 }
 }
 else if ((t1 - l / e1_l) > 0 && t2 < 1)
 {
 for (int k = 0; k < 3; ++k)
 {
 connection.p4[k] = connection.p1[k];
 connection.p3[k] = connection.p2[k];

 connection.p1[k] -= l * e1_v[k];
 connection.p2[k] += l * e2_v[k];

 connection.p2[k] = e2_p1[k];
 }
 }
 else
 {
 ROS_ERROR_STREAM("mergeConvexPolygons: edge is too short");
 //return false;
 }
 connection_list.push_back(connection);
 }
 // merge polygons/
 vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
 vtkSmartPointer<vtkCellArray> polygon_array = vtkSmartPointer<vtkCellArray>::New();
 vtkSmartPointer<vtkPolygon> polygon = vtkSmartPointer<vtkPolygon>::New();

 std::stack<Connection> connection_stack;

 vtkIdType current_polygon_id = 0;
 vtkIdType current_edge_id = 0;
 double p[3];
 polygon_source[0]->GetCell(0)->GetPoints()->GetPoint(0, p);

 polygon->GetPointIds()->InsertNextId(points->GetNumberOfPoints());
 points->InsertNextPoint(p);

 vtkIdType n_points = polygon_source[current_polygon_id]->GetNumberOfPoints();

 // find line
 int line_id = findConnectionLine(polygon_source, connection_list, current_polygon_id, current_edge_id);
 if (line_id == -1)
 {
 ROS_ERROR_STREAM("mergeConvexPolygons: Error");
 return false;
 }
 connection_stack.push(connection_list[line_id]);
 connection_list.erase(connection_list.begin() + line_id);
 while (!connection_stack.empty())
 {

 vtkIdType final_edge_id;
 if (connection_stack.top().polygon_id_1 == current_polygon_id)
 final_edge_id = connection_stack.top().edge_id_1;
 else
 final_edge_id = connection_stack.top().edge_id_2;
 vtkIdType n_saved_points = (n_points + final_edge_id - current_edge_id) % n_points;

 for (vtkIdType i(0); i < n_saved_points; ++i)
 {
 polygon_source[current_polygon_id]->GetCell(0)->GetPoints()->GetPoint((i + 1 + current_edge_id) % n_points, p);
 polygon->GetPointIds()->InsertNextId(points->GetNumberOfPoints());
 points->InsertNextPoint(p);
 }

 if (connection_stack.top().polygon_id_1 == current_polygon_id)
 {
 polygon->GetPointIds()->InsertNextId(points->GetNumberOfPoints());
 points->InsertNextPoint(connection_stack.top().p1);
 polygon->GetPointIds()->InsertNextId(points->GetNumberOfPoints());
 points->InsertNextPoint(connection_stack.top().p2);

 current_polygon_id = connection_stack.top().polygon_id_2;
 n_points = polygon_source[current_polygon_id]->GetNumberOfPoints();
 current_edge_id = connection_stack.top().edge_id_2;
 }
 else
 {
 polygon->GetPointIds()->InsertNextId(points->GetNumberOfPoints());
 points->InsertNextPoint(connection_stack.top().p3);
 polygon->GetPointIds()->InsertNextId(points->GetNumberOfPoints());
 points->InsertNextPoint(connection_stack.top().p4);

 current_polygon_id = connection_stack.top().polygon_id_1;
 n_points = polygon_source[current_polygon_id]->GetNumberOfPoints();
 current_edge_id = connection_stack.top().edge_id_1;
 }
 if (!connection_stack.top().second_time)
 connection_stack.top().second_time = true;
 else
 connection_stack.pop();
 line_id = findConnectionLine(polygon_source, connection_list, current_polygon_id, current_edge_id);
 if (line_id != -1)
 {
 connection_stack.push(connection_list[line_id]);
 connection_list.erase(connection_list.begin() + line_id);
 }
 else
 {
 current_edge_id = (current_edge_id + 1) % n_points;

 polygon_source[current_polygon_id]->GetCell(0)->GetPoints()->GetPoint(current_edge_id, p);
 polygon->GetPointIds()->InsertNextId(points->GetNumberOfPoints());
 points->InsertNextPoint(p);
 }
 }
 for (vtkIdType i(current_edge_id); i < polygon_source[0]->GetNumberOfPoints(); ++i)
 {
 polygon_source[0]->GetCell(0)->GetPoints()->GetPoint(i, p);
 polygon->GetPointIds()->InsertNextId(points->GetNumberOfPoints());
 points->InsertNextPoint(p);
 }
 polygon_array->InsertNextCell(polygon);
 vtkSmartPointer<vtkPolyData> poly_data = vtkSmartPointer<vtkPolyData>::New();
 poly_data->SetPolys(polygon_array);
 poly_data->SetPoints(points);

 polygon_source.clear();

 polygon_source.push_back(poly_data);
 return true;
 }
 int DonghongDing::findConnectionLine(Polygons polygon_source, std::vector<Connection> &connection_list,
 vtkIdType current_polygon_id, vtkIdType current_edge_id)
 {
 int line_id = -1;
 vtkIdType aux_edge_id = VTK_ID_MAX;
 for (unsigned i(0); i < connection_list.size(); ++i)
 {

 vtkIdType local_edge_id;
 vtkIdType n_points = polygon_source[current_polygon_id]->GetCell(0)->GetNumberOfPoints();
 if (current_polygon_id == connection_list[i].polygon_id_1 )
 {
 if (connection_list[i].edge_id_1 < current_edge_id)
 local_edge_id = connection_list[i].edge_id_1 + n_points;
 else
 local_edge_id = connection_list[i].edge_id_1;

 if (local_edge_id < aux_edge_id)
 {
 aux_edge_id = local_edge_id;
 line_id = i;
 }
 else if (local_edge_id == aux_edge_id)
 {
 double edge_1[3];
 polygon_source[connection_list[i].polygon_id_1]->GetCell(0)->GetPoints()->GetPoint(connection_list[i].edge_id_1,
 edge_1);
 if (vtkMath::Distance2BetweenPoints(edge_1, connection_list[i].p1)
 < vtkMath::Distance2BetweenPoints(edge_1, connection_list[line_id].p1))
 {
 aux_edge_id = local_edge_id;
 line_id = i;
 }
 }
 }
 if (current_polygon_id == connection_list[i].polygon_id_2 )
 {
 if (connection_list[i].edge_id_2 < current_edge_id)
 local_edge_id = connection_list[i].edge_id_2 + n_points;
 else
 local_edge_id = connection_list[i].edge_id_2;

 if (local_edge_id < aux_edge_id)
 {
 aux_edge_id = local_edge_id;
 line_id = i;
 }
 else if (local_edge_id == aux_edge_id)
 {
 double edge_2[3];
 polygon_source[connection_list[i].polygon_id_2]->GetCell(0)->GetPoints()->GetPoint(connection_list[i].edge_id_2,
 edge_2);
 if (vtkMath::Distance2BetweenPoints(edge_2, connection_list[i].p3)
 < vtkMath::Distance2BetweenPoints(edge_2, connection_list[line_id].p3))
 {
 aux_edge_id = local_edge_id;
 line_id = i;
 }
 }
 }
 }
 return line_id;
 }
 */

bool DonghongDing::generateTrajectoryInConvexPolygon(const Polygon poly_data)
{
  semaphore.wait(); //Semaphore

  removeDuplicatePoints(poly_data);
  mergeColinearEdges(poly_data);

  if (!offsetPolygonContour(poly_data, deposited_material_width_ / 2.0))
  {
    semaphore.signal();
    return false;
  };

  vtkIdType edge_id;
  vtkIdType opposite_point_id;
  double h;

  h = identifyZigzagDirection(poly_data, edge_id, opposite_point_id);
  if (h / deposited_material_width_ <= 4)
    ROS_WARN_STREAM("warning: h/d <= 4 | h/d = " << h / deposited_material_width_);

  vtkSmartPointer<vtkPoints> left_chain = vtkSmartPointer<vtkPoints>::New();
  vtkSmartPointer<vtkPoints> right_chain = vtkSmartPointer<vtkPoints>::New();
  vtkSmartPointer<vtkPoints> zigzag_points = vtkSmartPointer<vtkPoints>::New();

  identifyLeftChain(poly_data, edge_id, opposite_point_id, left_chain, right_chain);
  if (!offsetLeftChain(poly_data, edge_id, opposite_point_id, left_chain, right_chain))
  {
    semaphore.signal();
    return false;
  }

  if (!offsetPolygonContour(poly_data, deposited_material_width_))
  {
    semaphore.signal();
    return false;
  }

  if (!zigzagGeneration(poly_data, edge_id, opposite_point_id, zigzag_points, deposited_material_width_))
  {
    semaphore.signal();
    return false;
  }

  mergeListOfPoints(poly_data, left_chain, right_chain, zigzag_points);
  removeDuplicatePoints(poly_data);
  mergeColinearEdges(poly_data);
  semaphore.signal();
  return true;
}

std::string DonghongDing::connectMeshLayers(std::vector<Layer> &layers,
                                            ram_msgs::AdditiveManufacturingTrajectory &msg)
{
  // 1. divide in groups with the same number of polygons
  // 2. divide this groups in layer with only one polygon
  // 3. connect the simple layers

  //std::vector<Layer> layers_vector;
  unsigned first_layer = 0;
  unsigned n_polygons = layers[0].size();

  if (n_polygons != 1)
    return "connectMeshLayers: function to connect layers with several polygons is not implemented";

  for (unsigned layer_id(0); layer_id < layers.size(); ++layer_id)
  {
    if (layers[layer_id].size() != n_polygons)
    {
      return "connectMeshLayers: function to connect layers with several polygons is not implemented";

      //send layers_vector
      //divideInLayersWithOnePolygon(layers_vector,msg,first_layer,slicing_direction);
      //layers_vector.clear();
      //first_layer = layer_id;
      //n_polygons = layers[layer_id].size();
    }
    //layers_vector.push_back(layers[layer_id]);
  }
  //send layers_vector
  //divideInLayersWithOnePolygon(layers_vector,msg,first_layer,slicing_direction);
  connectLayersWithOnePolygon(layers, msg, first_layer);
  return "";
}

void DonghongDing::divideInLayersWithOnePolygon(std::vector<Layer> &layers,
                                                ram_msgs::AdditiveManufacturingTrajectory &msg,
                                                const unsigned first_layer)
{
  std::vector<Layer> layers_vector;
  Layer current_layer;
  unsigned n_polygons = layers[0].size();
  vtkSmartPointer<vtkCenterOfMass> center_of_mass_filter = vtkSmartPointer<vtkCenterOfMass>::New();

  while (n_polygons > 0)
  {
    double center_0[3];
    double center_1[3];
    // Compute the center of mass
    center_of_mass_filter->SetInputData(layers[0][0][0]);
    center_of_mass_filter->SetUseScalarsAsWeights(false);
    center_of_mass_filter->Update();
    center_of_mass_filter->GetCenter(center_0);

    current_layer.clear();
    layers_vector.clear();
    current_layer.push_back(layers[0][0]); //layer with only 1 polygon
    layers_vector.push_back(current_layer);

    layers[0].erase(layers[0].begin());

    for (unsigned layer_id(1); layer_id < layers.size(); ++layer_id)
    {
      unsigned closest_polygon_vector_id = 0;
      double min_distance = DBL_MAX;
      for (unsigned polygon_vector_id(1); polygon_vector_id < layers[layer_id].size(); ++polygon_vector_id)
      {
        // Compute the center of mass
        center_of_mass_filter->SetInputData(layers[layer_id][polygon_vector_id][0]);
        center_of_mass_filter->SetUseScalarsAsWeights(false);
        center_of_mass_filter->Update();
        center_of_mass_filter->GetCenter(center_1);

        double distance = sqrt(vtkMath::Distance2BetweenPoints(center_0, center_1));
        if (distance < min_distance)
        {
          min_distance = distance;
          closest_polygon_vector_id = polygon_vector_id;
        }
      }
      //
      current_layer.clear();
      current_layer.push_back(layers[layer_id][closest_polygon_vector_id]); //layer with only 1 polygon
      layers_vector.push_back(current_layer);

      layers[layer_id].erase(layers[layer_id].begin() + closest_polygon_vector_id);
    }
    //send layers
    connectLayersWithOnePolygon(layers_vector, msg, first_layer);
    n_polygons--;

  } //--- end while

}

void DonghongDing::connectLayersWithOnePolygon(std::vector<Layer> &layers,
                                               ram_msgs::AdditiveManufacturingTrajectory &msg,
                                               const unsigned first_layer)
{
  unsigned n_layers(layers.size());

  // layers are connected close to this line
  double l1[3]; // Point in the plane of the first layer
  double l2[3]; // Point in the last layer

  double p_first_layer[3]; // Point in the first layer

  layers.back()[0][0]->GetCell(0)->GetPoints()->GetPoint(0, l2);
  //find l1
  double l2_p[3]; // Vector between l2 and p_first layer
  vtkMath::Subtract(p_first_layer, l2, l2_p);
  double l2_l1[3]; // Vector between l2 and l1
  vtkMath::ProjectVector(l2_p, normal_vector_, l2_l1);
  vtkMath::Add(l2, l2_l1, l1); //l1

  for (unsigned layer_id(0); layer_id < n_layers; ++layer_id)
  {
    vtkIdType n_points = layers[layer_id][0][0]->GetCell(0)->GetNumberOfPoints();
    //vtkIdType closest_point_id = 0;
    vtkIdType closest_edge_id = 0;
    double connection_point[3] = {0, 0, 0};
    double shortest_distance = DBL_MAX;
    bool is_close_to_a_vertex = false;

    // Find the closest point to the line between the points l1 and l2
    for (vtkIdType id(0); id < n_points; ++id)
    {
      double e_p0[3];
      double e_p1[3];
      layers[layer_id][0][0]->GetCell(0)->GetEdge(id)->GetPoints()->GetPoint(0, e_p0);
      layers[layer_id][0][0]->GetCell(0)->GetEdge(id)->GetPoints()->GetPoint(1, e_p1);
      double closestPt1[3];
      double closestPt2[3];
      double t1;
      double t2;
      double distance_to_line = vtkLine::DistanceBetweenLineSegments(l1, l2, e_p0, e_p1, closestPt1, closestPt2, t1,
                                                                     t2);

      if (distance_to_line < shortest_distance)
      {
        shortest_distance = distance_to_line;

        if (sqrt(vtkMath::Distance2BetweenPoints(e_p0, closestPt2)) < deposited_material_width_ / 2) // Close to e_p0
        {
          closest_edge_id = id;

          connection_point[0] = e_p0[0];
          connection_point[1] = e_p0[1];
          connection_point[2] = e_p0[2];
          is_close_to_a_vertex = true;
        }
        else if (sqrt(vtkMath::Distance2BetweenPoints(e_p1, closestPt2)) < deposited_material_width_ / 2) // Close to e_p1
        {
          closest_edge_id = id + 1;

          connection_point[0] = e_p1[0];
          connection_point[1] = e_p1[1];
          connection_point[2] = e_p1[2];
          is_close_to_a_vertex = true;
        }
        else
        {
          closest_edge_id = id;

          connection_point[0] = closestPt2[0];
          connection_point[1] = closestPt2[1];
          connection_point[2] = closestPt2[2];
          is_close_to_a_vertex = false;
        }
      }
    }
    //save the layer points in the msg
    Eigen::Isometry3d first_pose_isometry = Eigen::Isometry3d::Identity();
    ram_msgs::AdditiveManufacturingPose first_pose_srv;

    first_pose_isometry.translation()[0] = connection_point[0];
    first_pose_isometry.translation()[1] = connection_point[1];
    first_pose_isometry.translation()[2] = connection_point[2];
    tf::poseEigenToMsg(first_pose_isometry, first_pose_srv.pose);

    first_pose_srv.layer_level = layer_id + first_layer;
    first_pose_srv.layer_index = first_pose_srv.layer_level;

    if (layer_id == 0)      // first point
      first_pose_srv.polygon_start = true;
    msg.poses.push_back(first_pose_srv);

    for (vtkIdType i(0); i < n_points; ++i)
    {
      double p[3];
      layers[layer_id][0][0]->GetCell(0)->GetPoints()->GetPoint((closest_edge_id + i + 1) % n_points, p);
      Eigen::Isometry3d pose_isometry = Eigen::Isometry3d::Identity();
      ram_msgs::AdditiveManufacturingPose pose_srv;

      pose_isometry.translation()[0] = p[0];
      pose_isometry.translation()[1] = p[1];
      pose_isometry.translation()[2] = p[2];
      tf::poseEigenToMsg(pose_isometry, pose_srv.pose);

      pose_srv.layer_level = layer_id + first_layer;
      pose_srv.layer_index = pose_srv.layer_level;

      msg.poses.push_back(pose_srv);
    }
    if (!is_close_to_a_vertex)
    {
      first_pose_srv.polygon_start = false;
      first_pose_srv.layer_level = layer_id + first_layer;
      msg.poses.push_back(first_pose_srv);
    }
  }
  msg.poses.back().polygon_end = true;      // last point
}

void DonghongDing::connectYamlLayers(const Layer &current_layer,
                                     ram_msgs::AdditiveManufacturingTrajectory &msg,
                                     const double number_of_layers,
                                     const double height_between_layers,
                                     const std::array<double, 3> offset_direction)
{
  double offset_dir[3] = {offset_direction[0], offset_direction[1], offset_direction[2]};

  if (vtkMath::Norm(offset_dir) == 0)
  {
    ROS_ERROR_STREAM("connectYamlLayers: offset direction is not valid");
    return;
  }
  vtkMath::Normalize(offset_dir);

  unsigned layer_index(0);
  for (auto polygon_vector : current_layer)
  {
    for (auto polygon : polygon_vector)
    {
      vtkIdType n_points = polygon->GetNumberOfPoints();
      for (vtkIdType layer_id(0); layer_id < number_of_layers; ++layer_id)
      {
        double offset_vector[3];
        offset_vector[0] = height_between_layers * layer_id * offset_dir[0];
        offset_vector[1] = height_between_layers * layer_id * offset_dir[1];
        offset_vector[2] = height_between_layers * layer_id * offset_dir[2];

        for (vtkIdType point_id(0); point_id <= n_points; ++point_id)
        {
          double p[3];
          Eigen::Isometry3d pose_isometry = Eigen::Isometry3d::Identity();
          polygon->GetCell(0)->GetPoints()->GetPoint(point_id % n_points, p);

          pose_isometry.translation()[0] = (p[0] + offset_vector[0]);
          pose_isometry.translation()[1] = (p[1] + offset_vector[1]);
          pose_isometry.translation()[2] = (p[2] + offset_vector[2]);

          ram_msgs::AdditiveManufacturingPose pose_srv;
          tf::poseEigenToMsg(pose_isometry, pose_srv.pose);
          pose_srv.layer_level = layer_id;
          pose_srv.layer_index = layer_index;

          if (layer_id == 0 && point_id == 0) // First point
          {
            pose_srv.polygon_start = true;
          }
          msg.poses.push_back(pose_srv);
        }
        ++layer_index;
      }
      msg.poses.back().polygon_end = true;  // Last point
    }
  }
}
