#include <string>
#include <strings.h>

#include <eigen_conversions/eigen_msg.h>
#include <ram_msgs/AdditiveManufacturingTrajectory.h>
#include <ram_path_planning/GenerateTrajectory.h>
#include <ram_path_planning/donghong_ding.hpp>
#include <ram_path_planning/mesh_slicer.hpp>
#include <ros/ros.h>
#include <visualization_msgs/Marker.h>

#include <unique_id/unique_id.h>

#include <vtkCommand.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkRendererCollection.h>
#include <vtkTransform.h>
#include <vtkTransformFilter.h>

bool use_gui;

std::shared_ptr<ros::NodeHandle> nh;
DonghongDing traj_generator;
ros::Publisher traj_pub;

typedef DonghongDing::Polygon Polygon;
typedef DonghongDing::PolygonVector PolygonVector;
typedef DonghongDing::Layer Layer;

// DonghongDing Generated trajectory
std::vector<Layer> additive_traj;

Layer current_layer;
vtkSmartPointer<vtkPolyData> mesh = vtkSmartPointer<vtkPolyData>::New();
vtkSmartPointer<vtkStripper> stripper;

class vtkRendererUpdaterTimer : public vtkCommand
{
public:
  static vtkRendererUpdaterTimer *New()
  {
    vtkRendererUpdaterTimer *cb = new vtkRendererUpdaterTimer;
    return cb;
  }

  virtual void Execute(vtkObject *caller,
                       unsigned long,
                       void * vtkNotUsed(callData))
  {
    vtkRenderWindowInteractor *iren = vtkRenderWindowInteractor::SafeDownCast(caller);
    vtkRenderer * renderer = iren->GetRenderWindow()->GetRenderers()->GetFirstRenderer();

    int representation(1);
    vtkActor* last_actor(renderer->GetActors()->GetLastActor());

    if (last_actor)
      representation = last_actor->GetProperty()->GetRepresentation();

    renderer->RemoveAllViewProps(); // Remove all actors

    // Add mesh
    if (mesh)
    {
      vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
      mapper->SetInputData(mesh);

      vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
      actor->SetMapper(mapper);

      actor->GetProperty()->SetColor(0.5, 0.5, 0.5);
      actor->GetProperty()->SetOpacity(0.5);
      actor->GetProperty()->SetRepresentation(representation);
      renderer->AddActor(actor);
    }

    if (stripper)
    {
      vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
      mapper->SetInputConnection(stripper->GetOutputPort());
      mapper->ScalarVisibilityOff();

      vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
      actor->SetMapper(mapper);

      actor->GetProperty()->SetColor(1, 0, 0);
      actor->GetProperty()->SetLineWidth(2.0);
      actor->GetProperty()->SetRepresentation(representation);
      renderer->AddActor(actor);
    }

    // Add all polygons
    for (unsigned i = 0; i < current_layer.size(); ++i)
    {
      unsigned color_index(0);
      for (auto polygon : current_layer[i])
      {
        vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
        mapper->SetInputData(polygon);

        vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
        actor->SetMapper(mapper);
        double rgb[3];
        getColorFromIndex(color_index++, rgb);

        actor->GetProperty()->SetColor(rgb);
        actor->GetProperty()->SetRepresentation(representation);
        renderer->AddActor(actor);
      }
    }
    iren->GetRenderWindow()->Render();
  }

private:
  void getColorFromIndex(const unsigned i,
                         double rgb[3])
  {
    switch (i % 6)
    {
      case 0:
        rgb[0] = 0.3;
        rgb[1] = 0.3;
        rgb[2] = 1;
        break;
      case 1:
        rgb[0] = 0.3;
        rgb[1] = 1;
        rgb[2] = 0.3;
        break;
      case 2:
        rgb[0] = 0.3;
        rgb[1] = 1;
        rgb[2] = 1;
        break;
      case 3:
        rgb[0] = 1;
        rgb[1] = 0.3;
        rgb[2] = 0.3;
        break;
      case 4:
        rgb[0] = 1;
        rgb[1] = 0.3;
        rgb[2] = 1;
        break;
      case 5:
        rgb[0] = 1;
        rgb[1] = 1;
        rgb[2] = 0.3;
        break;
      default:
        rgb[0] = 1;
        rgb[1] = 1;
        rgb[2] = 1;
        break;
    }
  }
};

std::string fileExtension(const std::string full_path)
{
  size_t last_index = full_path.find_last_of("/");
  std::string file_name = full_path.substr(last_index + 1, full_path.size());

  last_index = file_name.find_last_of("\\");
  file_name = file_name.substr(last_index + 1, file_name.size());

  last_index = file_name.find_last_of(".");
  if (last_index == std::string::npos)
    return "";

  return file_name.substr(last_index + 1, file_name.size());
}

bool generateTrajectoryCallback(ram_path_planning::GenerateTrajectory::Request &req,
                                ram_path_planning::GenerateTrajectory::Response &res)
{
  if (req.generation_params.file.empty())
  {
    res.error_msg = "File name cannot be empty";
    ROS_ERROR_STREAM(res.error_msg);
    return true;
  }

  if (req.generation_params.height_between_layers <= 0)
  {
    res.error_msg = "Height between layers cannot be <= 0";
    ROS_ERROR_STREAM(res.error_msg);
    return true;
  }

  if (req.generation_params.deposited_material_width <= 0)
  {
    res.error_msg = "Deposited material width cannot be <= 0";
    ROS_ERROR_STREAM(res.error_msg);
    return true;

  }
  if (req.generation_params.contours_filtering_tolerance < 0)
  {
    res.error_msg = "Contours filtering tolerance cannot be < 0";
    ROS_ERROR_STREAM(res.error_msg);
    return true;
  }

  bool is_yaml_file(false);
  const std::string file_extension(fileExtension(req.generation_params.file));
  if (!strcasecmp(file_extension.c_str(), "yaml") || !strcasecmp(file_extension.c_str(), "yml"))
  {
    is_yaml_file = true;
    if (req.generation_params.number_of_layers < 1)
    {
      res.error_msg = "Number of layers cannot be < 1";
      ROS_ERROR_STREAM(res.error_msg);
      return true;
    }
  }
  else if (strcasecmp(file_extension.c_str(), "obj") && strcasecmp(file_extension.c_str(), "ply")
      && strcasecmp(file_extension.c_str(), "stl"))
  {
    res.error_msg = "File is not a YAML, YML, OBJ, PLY or STL file: " + req.generation_params.file;
    ROS_ERROR_STREAM(res.error_msg);
    return true;
  }

  ram_msgs::AdditiveManufacturingTrajectory msg;
  //Add request parameter to the trajectory
  msg.generation_params = req.generation_params;
  msg.similar_layers = is_yaml_file;
  current_layer.clear();
  std::array<double, 3> slicing_direction;
  slicing_direction[0] = req.generation_params.slicing_direction.x;
  slicing_direction[1] = req.generation_params.slicing_direction.y;
  slicing_direction[2] = req.generation_params.slicing_direction.z;

  if (is_yaml_file)
  {
    std::string error_message;
    error_message = traj_generator.generateOneLayerTrajectory(req.generation_params.file, current_layer,
                                                              req.generation_params.deposited_material_width,
                                                              req.generation_params.contours_filtering_tolerance,
                                                              is_yaml_file, slicing_direction, M_PI / 6, false,
                                                              use_gui);
    if (!error_message.empty())

    {
      res.error_msg = error_message + "\n" + "generateOneLayerTrajectory: Error generating trajectory";
      ROS_ERROR_STREAM(res.error_msg);
      return true;
    }
    traj_generator.connectYamlLayers(current_layer, msg, req.generation_params.number_of_layers,
                                     req.generation_params.height_between_layers);
  }
  else
  {
    // Slice the mesh
    const unsigned nb_layers(
        ram_mesh_slicing::sliceMesh(additive_traj, req.generation_params.file, mesh, stripper,
                                    req.generation_params.height_between_layers, slicing_direction, use_gui));

    if (nb_layers < 1)
    {
      res.error_msg = "Slicing failed, zero slice generated";
      ROS_ERROR_STREAM(res.error_msg);
      return true;
    }

    // Generate trajectory on each layer
    for (unsigned i(0); i < additive_traj.size(); ++i)
    {
      Polygon contours(additive_traj[i][0][0]);
      contours->DeepCopy(additive_traj[i][0][0]);

      std::string error_message;
      error_message = traj_generator.generateOneLayerTrajectory(contours, additive_traj[i],
                                                                req.generation_params.deposited_material_width,
                                                                req.generation_params.contours_filtering_tolerance,
                                                                is_yaml_file, slicing_direction, M_PI / 6, false,
                                                                use_gui);

      if (!error_message.empty())
      {
        res.error_msg = error_message + "\n" + "Could not generate layer " + std::to_string(i);
        ROS_ERROR_STREAM(res.error_msg);
        return true;
      }
      current_layer = additive_traj[i];

    }

    std::string return_message;
    return_message = traj_generator.connectMeshLayers(additive_traj, msg);
    if (!return_message.empty())
    {
      res.error_msg = return_message + "\n" + "Error connecting layers";
      ROS_ERROR_STREAM(res.error_msg);
      return true;
    }
    //Update number of layers in trajectory
    msg.generation_params.number_of_layers = nb_layers;
  }

  // Trajectory is now complete
  // Create a message
  // Fill response and publish trajectory
  if (msg.poses.size() == 0)
  {
    res.error_msg = "Trajectory is empty";
    ROS_ERROR_STREAM(res.error_msg);
    return true;
  }
  res.number_of_poses = msg.poses.size();

  // Add UUID
  for (auto &pose : msg.poses)
    pose.unique_id = unique_id::toMsg(unique_id::fromRandom());

  // Set the time to zero, the trajectory is incomplete at this stage
  // It will be published on trajectory_tmp, completed by fill_trajectory and then published on trajectory
  msg.generated.nsec = 0;
  msg.generated.sec = 0;
  msg.modified = msg.generated;
  if (traj_pub.getNumSubscribers() == 0)
    ROS_ERROR_STREAM("No subscriber on topic " << traj_pub.getTopic() << ": trajectory is lost");
  traj_pub.publish(msg);

  return true;
}

int main(int argc,
         char **argv)
{
  ros::init(argc, argv, "path_planing");
  nh.reset(new ros::NodeHandle);
  ros::AsyncSpinner spinner(1);
  spinner.start();
  ros::ServiceServer service = nh->advertiseService("ram/path_planning/generate_trajectory",
                                                    generateTrajectoryCallback);
  traj_pub = nh->advertise<ram_msgs::AdditiveManufacturingTrajectory>("ram/trajectory_tmp", 1, true);

  nh->param<bool>("use_gui", use_gui, false);
  if (use_gui)
  {
    vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
    vtkSmartPointer<vtkRenderWindow> render_window = vtkSmartPointer<vtkRenderWindow>::New();
    render_window->AddRenderer(renderer);
    render_window->SetSize(800, 600);

    vtkSmartPointer<vtkRenderWindowInteractor> render_window_interactor =
        vtkSmartPointer<vtkRenderWindowInteractor>::New();
    render_window_interactor->SetRenderWindow(render_window);
    render_window_interactor->Initialize();

    vtkSmartPointer<vtkInteractorStyleTrackballCamera> image_style =
        vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New();
    render_window_interactor->SetInteractorStyle(image_style);

    vtkSmartPointer<vtkRendererUpdaterTimer> cb = vtkSmartPointer<vtkRendererUpdaterTimer>::New();
    render_window_interactor->AddObserver(vtkCommand::TimerEvent, cb);
    render_window_interactor->CreateRepeatingTimer(500);

    render_window_interactor->Start();
  }
  ros::waitForShutdown();
  spinner.stop();
  return 0;
}
