#ifndef RAM_TRAJECTORY_FILES_MANAGER_HPP
#define RAM_TRAJECTORY_FILES_MANAGER_HPP

#include <fstream>
#include <string>
#include <vector>

#include "yaml_utils.hpp"
#include <yaml-cpp/exceptions.h>
#include <yaml-cpp/mark.h>

#include <vtkPolyData.h>
#include <vtkPolygon.h>
#include <vtkSmartPointer.h>
#include <vtkVector.h>

class TrajectoryFilesManager
{
public:
  typedef vtkSmartPointer<vtkPolyData> Polygon;
  typedef std::vector<Polygon> Polygons;
  typedef std::vector<Polygons> PolygonsVector;

  TrajectoryFilesManager();

  static bool yamlFileToPolydata(const std::string yaml_file,
                                 Polygon poly_data);

  static bool polydataToYamlFile(const std::string yaml_file,
                                 const Polygon poly_data);
};

#endif

