#ifndef POST_PROCESSOR_HPP
#define POST_PROCESSOR_HPP

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <ros/ros.h>
#include <eigen_conversions/eigen_msg.h>
#include <ram_msgs/AdditiveManufacturingTrajectory.h>
#include <ram_utils/GetStartPose.h>

class PostProcessor
{
public:
  PostProcessor();

  virtual ~PostProcessor()
  {
  }

  virtual std::string
  getName();

  virtual std::string
  getDescription();

  virtual void
  addComment(const std::string comment);

  virtual void
  addPose(const ram_msgs::AdditiveManufacturingPose &pose);

  virtual bool
  beforeGenerating();

  virtual bool
  afterGenerating();

  /// Polygons
  virtual void
  startPolygonBefore();

  virtual void
  startPolygonAfter();

  virtual void
  finishPolygonBefore();

  virtual void
  finishPolygonAfter();

  /// Layer change
  virtual void
  layerIndexChanged(const unsigned layer_index,
                    const unsigned layer_level);

  /// Feed rate
  virtual void
  startFeedRateBefore(const double feed_rate);
  virtual void
  startFeedRateAfter(const double feed_rate);
  virtual void
  changeFeedRateBefore(const double feed_rate);
  virtual void
  changeFeedRateAfter(const double feed_rate);
  virtual void
  stopFeedRateBefore();
  virtual void
  stopFeedRateAfter();

  /// Laser power
  virtual void
  startLaserPowerBefore(const unsigned laser_power);
  virtual void
  startLaserPowerAfter(const unsigned laser_power);
  virtual void
  changeLaserPowerBefore(const unsigned laser_power);
  virtual void
  changeLaserPowerAfter(const unsigned laser_power);
  virtual void
  stopLaserPowerBefore();
  virtual void
  stopLaserPowerAfter();

  std::string
  generateProgram(const ram_msgs::AdditiveManufacturingTrajectory &trajectory);

  std::string program_name_ = "program";
  std::string program_comment_ = "comment";
  bool verbose_comments_ = false;

protected:
  ros::NodeHandle nh_;
  ram_msgs::AdditiveManufacturingTrajectory trajectory_;
  std::string program_;

private:
  ros::ServiceClient get_start_pose_client_;
  Eigen::Isometry3d start_pose_ = Eigen::Isometry3d::Identity();

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

#endif
