#include <ram_post_processor/post_processor.hpp>

PostProcessor::PostProcessor()
{
  get_start_pose_client_ = nh_.serviceClient<ram_utils::GetStartPose>("ram/get_start_pose");
}

std::string PostProcessor::getName()
{
  return "Generic";
}

std::string PostProcessor::getDescription()
{
  return "A generic post processor generating a human readable textual output.\n"
      "Not meant to be used by any kind of hardware.";
}

void PostProcessor::addComment(const std::string comment)
{
  program_.append("\n" + comment);
}

void PostProcessor::addPose(const ram_msgs::AdditiveManufacturingPose &pose)
{
  std::stringstream buffer;
  buffer << pose.pose;
  std::string pose_str(buffer.str());
  program_.append("\n Pose: \n" + buffer.str());
}

bool PostProcessor::beforeGenerating()
{
  program_.append("\nProgram name: " + program_name_);
  program_.append("\nProgram comment: " + program_comment_);
  return true;
}

bool PostProcessor::afterGenerating()
{
  return true;
}

/// Polygons
void PostProcessor::startPolygonBefore()
{
  program_.append("\nStart polygon");
}

void PostProcessor::startPolygonAfter()
{
}

void PostProcessor::finishPolygonBefore()
{
}

void PostProcessor::finishPolygonAfter()
{
  program_.append("\nFinish polygon");
}

/// Layer change
void PostProcessor::layerIndexChanged(const unsigned layer_index, const unsigned layer_level)
{
  program_.append("\nLayer index " + std::to_string(layer_index));
  program_.append("\nLayer level " + std::to_string(layer_level));
}

/// Feed rate
void PostProcessor::startFeedRateBefore(const double feed_rate)
{
  program_.append("\nStart feed rate: " + std::to_string(feed_rate));
}

void PostProcessor::startFeedRateAfter(const double)
{
}

void PostProcessor::changeFeedRateBefore(const double feed_rate)
{
  program_.append("\nChange feed rate: " + std::to_string(feed_rate));
}

void PostProcessor::changeFeedRateAfter(const double)
{
}

void PostProcessor::stopFeedRateBefore()
{
  program_.append("\nStop feed rate");
}

void PostProcessor::stopFeedRateAfter()
{
  program_.append("\nStop feed rate");
}

/// Laser power
void PostProcessor::startLaserPowerBefore(const unsigned laser_power)
{
  program_.append("\nStart laser_power: " + std::to_string(laser_power));
}

void PostProcessor::startLaserPowerAfter(const unsigned)
{
}

void PostProcessor::changeLaserPowerBefore(const unsigned laser_power)
{
  program_.append("\nChange laser power: " + std::to_string(laser_power));
}

void PostProcessor::changeLaserPowerAfter(const unsigned)
{
}

void PostProcessor::stopLaserPowerBefore()
{
  program_.append("\nStop laser power");
}

void PostProcessor::stopLaserPowerAfter()
{
}

std::string PostProcessor::generateProgram(const ram_msgs::AdditiveManufacturingTrajectory &trajectory)
{
  ram_msgs::AdditiveManufacturingPose last_pose;

  if (trajectory.poses.empty())
    return "Trajectory is empty";

  trajectory_ = trajectory;

  /// Get start pose
  // Check that service exists
  if (!get_start_pose_client_.waitForExistence(ros::Duration(0.5)))
    return "Cannot get start pose, service does not exist";

  // Call service to get start pose
  ram_utils::GetStartPose srv;
  if (!get_start_pose_client_.call(srv))
    return "Cannot get start pose, call to service failed";

  tf::poseMsgToEigen(srv.response.pose, start_pose_);

  if (!beforeGenerating())
    return "Error in beforeGenerating";

  bool first_pose(true);
  for (auto ram_pose : trajectory_.poses)
  {
    /// Actions BEFORE adding pose

    // (Layer index changed OR first pose) AND verbose comments
    if ((ram_pose.layer_index != last_pose.layer_index || first_pose) && verbose_comments_)
      layerIndexChanged(ram_pose.layer_index, ram_pose.layer_level);

    if (ram_pose.polygon_start)
      startPolygonBefore();

    if (ram_pose.polygon_end)
      finishPolygonBefore();

    if (first_pose)
    {
      if (ram_pose.params.feed_rate != 0)
        startFeedRateBefore(ram_pose.params.feed_rate);

      if (last_pose.params.laser_power != 0)
        startLaserPowerBefore(ram_pose.params.laser_power);
    }
    else
    {
      // Feed rate changes
      if (ram_pose.params.feed_rate != last_pose.params.feed_rate)
      {
        if (last_pose.params.feed_rate != 0 && ram_pose.params.feed_rate == 0)
          stopFeedRateBefore();
        else if (last_pose.params.feed_rate == 0)
          startFeedRateBefore(ram_pose.params.feed_rate);
        else
          changeFeedRateBefore(ram_pose.params.feed_rate);
      }

      // Laser power changes
      if (ram_pose.params.laser_power != last_pose.params.laser_power)
      {
        if (last_pose.params.laser_power != 0 && ram_pose.params.laser_power == 0)
          stopLaserPowerBefore();
        else if (last_pose.params.laser_power == 0)
          startLaserPowerBefore(ram_pose.params.laser_power);
        else
          changeLaserPowerBefore(ram_pose.params.laser_power);
      }
    }

    /// Add pose
    {
      Eigen::Isometry3d pose;
      // geometry_msg to Eigen
      tf::poseMsgToEigen(ram_pose.pose, pose);
      // Transform pose
      pose = start_pose_ * pose;
      // Eigen to geometry
      ram_msgs::AdditiveManufacturingPose ram_pose_tranformed(ram_pose);
      tf::poseEigenToMsg(pose, ram_pose_tranformed.pose);

      // Add transformed pose
      addPose(ram_pose_tranformed);
    }

    /// Actions AFTER adding pose
    if (ram_pose.polygon_start)
      startPolygonAfter();

    if (ram_pose.polygon_end)
      finishPolygonAfter();

    if (first_pose)
    {
      if (ram_pose.params.feed_rate != 0)
        startFeedRateAfter(ram_pose.params.feed_rate);

      if (last_pose.params.laser_power != 0)
        startLaserPowerAfter(ram_pose.params.laser_power);
    }
    else
    {
      // Feed rate changes
      if (ram_pose.params.feed_rate != last_pose.params.feed_rate)
      {
        if (last_pose.params.feed_rate != 0 && ram_pose.params.feed_rate == 0)
          stopFeedRateAfter();
        else if (last_pose.params.feed_rate == 0)
          startFeedRateAfter(ram_pose.params.feed_rate);
        else
          changeFeedRateAfter(ram_pose.params.feed_rate);
      }

      // Laser power changes
      if (ram_pose.params.laser_power != last_pose.params.laser_power)
      {
        if (last_pose.params.laser_power != 0 && ram_pose.params.laser_power == 0)
          stopLaserPowerAfter();
        else if (last_pose.params.laser_power == 0)
          startLaserPowerAfter(ram_pose.params.laser_power);
        else
          changeLaserPowerAfter(ram_pose.params.laser_power);
      }
    }

    first_pose = false;
    last_pose = ram_pose;
  }

  if (!afterGenerating())
    return "Error in afterGenerating";

  return program_;
}
