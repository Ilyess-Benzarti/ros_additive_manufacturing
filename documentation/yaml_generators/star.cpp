#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <fstream>
#include <cmath>

using std::cout;
using std::endl;
using std::cerr;

int main(int argc,
         char *argv[])
{
  if (argc < 1 + 3)
  {
    cerr << "Usage: " << argv[0] << "\033[94m"
        << " inner_radius \033[95m outer_radius \033[94m number_of_branches " << "\033[39m"
        << endl;
    return 1;
  }

  const double inner_radius(std::stof(argv[1]));
  const double outer_radius(std::stof(argv[2]));
  const unsigned number_of_branches(std::stof(argv[3]));
  cout << "Inner radius is " << inner_radius << " and outer radius is " << outer_radius << endl;
  cout << "Number of branches is " << number_of_branches << endl;

  std::vector<double[3]> outer_points(number_of_branches);
  double angle(0);
  for (auto point : outer_points)
  {
    point[0] = outer_radius * cos(angle);
    point[1] = outer_radius * sin(angle);
    point[2] = 0;
    angle += (2 * M_PI) / number_of_branches;
  }

  std::vector<double[3]> inner_points(number_of_branches);
  angle = (2 * M_PI) / number_of_branches / 2.0;
  for (auto point : inner_points)
  {
    point[0] = inner_radius * cos(angle);
    point[1] = inner_radius * sin(angle);
    point[2] = 0;
    angle += (2 * M_PI) / number_of_branches;
  }

  // Open file for writing
  const std::string file_name("star.yaml");
  std::ofstream yaml_file;
  yaml_file.open(file_name);
  yaml_file << "---\n"
      "layer:\n"
      "  -\n"
      "    polygon:\n";

  for (unsigned i(0); i < outer_points.size(); ++i)
  {
    yaml_file << "    - point: [" << outer_points[i][0] << ", " << outer_points[i][1] << ", " << outer_points[i][2]
        << "]\n";
    yaml_file << "    - point: [" << inner_points[i][0] << ", " << inner_points[i][1] << ", " << inner_points[i][2]
        << "]\n";
  }

  yaml_file << "\n";
  yaml_file.close();
  cout << "File " << file_name << " has been written" << endl;
  return 0;
}
