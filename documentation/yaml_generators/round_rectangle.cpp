#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <fstream>
#include <cmath>

using std::cout;
using std::endl;
using std::cerr;

int main(int argc,
         char *argv[])
{
  if (argc < 1 + 4)
  {
    cerr << "Usage: " << argv[0] << "\033[94m" <<
        " radius \033[95m points_in_circle_segment \033[94m length \033[95m witdh " << "\033[39m"  << endl;
    return 1;
  }

  const double radius(std::stof(argv[1]));
  const unsigned points_in_circle_segment(std::stof(argv[2]));
  const double length(std::stof(argv[3]));
  const double width(std::stof(argv[4]));
  cout << "Radius is " << radius << endl <<
          "Points in circle segment is " << points_in_circle_segment << endl <<
          "Length is " << length << endl <<
          "Width is " << width << endl;

  std::vector<std::array<double, 3>> points;
  std::array<double, 3> point;

  double angle(0);
  for (unsigned i(0); i < points_in_circle_segment; ++i)
  {
    point[0] = radius * cos(angle) + length - radius;
    point[1] = radius * sin(angle) + width - radius;
    point[2] = 0;
    points.push_back(point);
    angle += (M_PI * 0.5) / (points_in_circle_segment - 1);
  }
  angle = M_PI * 0.5;
  for (unsigned i(0); i < points_in_circle_segment; ++i)
  {
    point[0] = radius * cos(angle) + radius;
    point[1] = radius * sin(angle) + width - radius;
    point[2] = 0;
    points.push_back(point);
    angle += (M_PI * 0.5) / (points_in_circle_segment - 1);
  }
  angle = M_PI;
  for (unsigned i(0); i < points_in_circle_segment; ++i)
  {
    point[0] = radius * cos(angle) + radius;
    point[1] = radius * sin(angle) + radius;
    point[2] = 0;
    points.push_back(point);
    angle += (M_PI * 0.5) / (points_in_circle_segment - 1);
  }
  angle = M_PI * 1.5;
  for (unsigned i(0); i < points_in_circle_segment; ++i)
  {
    point[0] = radius * cos(angle) + length - radius;
    point[1] = radius * sin(angle) + radius;
    point[2] = 0;
    points.push_back(point);
    angle += (M_PI * 0.5) / (points_in_circle_segment - 1);
  }

  // Open file for writing
  const std::string file_name("round_rectangle.yaml");
  std::ofstream yaml_file;
  yaml_file.open(file_name);
  yaml_file << "---\n"
      "layer:\n"
      "  -\n"
      "    polygon:\n";

  for (auto point : points)
    yaml_file << "    - point: [" << point[0] << ", " << point[1] << ", " << point[2] << "]\n";

  yaml_file << "\n";
  yaml_file.close();
  cout << "File " << file_name << " has been written" << endl;
  return 0;
}
