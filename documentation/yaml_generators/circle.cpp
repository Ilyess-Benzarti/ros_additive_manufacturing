#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <fstream>
#include <cmath>

using std::cout;
using std::endl;
using std::cerr;

int main(int argc,
         char *argv[])
{
  if (argc < 1 + 2)
  {
    cerr << "Usage: " << argv[0] << "\033[94m"
        << " radius \033[95m number_of_points" << "\033[39m" << endl;
    return 1;
  }

  const double radius(std::stof(argv[1]));
  const unsigned number_of_points(std::stof(argv[2]));
  cout << "Radius is " << radius << " and number of points is " << number_of_points << endl;

  std::vector<double[3]> points(number_of_points);
  double angle(0);
  for (auto point : points)
  {
    point[0] = radius * cos(angle);
    point[1] = radius * sin(angle);
    point[2] = 0;
    angle += (2 * M_PI) / number_of_points;
  }

  // Open file for writing
  const std::string file_name("circle.yaml");
  std::ofstream yaml_file;
  yaml_file.open(file_name);
  yaml_file << "---\n"
      "layer:\n"
      "  -\n"
      "    polygon:\n";

  for (auto point : points)
    yaml_file << "    - point: [" << point[0] << ", " << point[1] << ", " << point[2] << "]\n";

  yaml_file << "\n";
  yaml_file.close();
  cout << "File " << file_name << " has been written" << endl;
  return 0;
}
