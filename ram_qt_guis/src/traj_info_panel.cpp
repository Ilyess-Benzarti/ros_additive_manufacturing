#include <ram_qt_guis/traj_info_panel.hpp>

namespace ram_qt_guis
{

TrajInfo::TrajInfo(QWidget* parent) :
        rviz::Panel(parent)
{
  this->setObjectName("Trajectory info");

  generated_ = new QLabel;
  modified_ = new QLabel;
  similar_layers_ = new QLabel;
  number_of_layers_levels_ = new QLabel;
  number_of_layers_indices_ = new QLabel;
  number_of_polygons_ = new QLabel;
  number_of_poses_ = new QLabel;
  trajectory_length_ = new QLabel;
  execution_time_ = new QLabel;
  wire_length_ = new QLabel;
  powder_weight_ = new QLabel;

  QVBoxLayout *scroll_widget_layout = new QVBoxLayout();
  QWidget *scroll_widget = new QWidget;
  scroll_widget->setLayout(scroll_widget_layout);
  QScrollArea *scroll_area = new QScrollArea;
  scroll_area->setWidget(scroll_widget);
  scroll_area->setWidgetResizable(true);
  scroll_area->setFrameShape(QFrame::NoFrame);
  QVBoxLayout* main_layout = new QVBoxLayout(this);
  main_layout->addWidget(scroll_area);

  QGridLayout* grid_layout = new QGridLayout;
  scroll_widget_layout->addLayout(grid_layout);

  grid_layout->addWidget(new QLabel("Generated at:"), 0, 0);
  grid_layout->addWidget(generated_, 0, 1);

  grid_layout->addWidget(new QLabel("Modified at:"));
  grid_layout->addWidget(modified_);

  grid_layout->addWidget(new QLabel("Similar layers:"));
  grid_layout->addWidget(similar_layers_);

  grid_layout->addWidget(new QLabel("Number of layers levels:"));
  grid_layout->addWidget(number_of_layers_levels_);

  grid_layout->addWidget(new QLabel("Number of layers indices:"));
  grid_layout->addWidget(number_of_layers_indices_);

  grid_layout->addWidget(new QLabel("Number of polygons:"));
  grid_layout->addWidget(number_of_polygons_);

  grid_layout->addWidget(new QLabel("Number of poses:"));
  grid_layout->addWidget(number_of_poses_);

  grid_layout->addWidget(new QLabel("Trajectory length:"));
  grid_layout->addWidget(trajectory_length_);

  grid_layout->addWidget(new QLabel("Execution time:"));
  grid_layout->addWidget(execution_time_);

  grid_layout->addWidget(new QLabel("Wire length:"));
  grid_layout->addWidget(wire_length_);

  grid_layout->addWidget(new QLabel("Powder weight:"));
  grid_layout->addWidget(powder_weight_);

  sub_ = nh_.subscribe("ram/information/trajectory", 1, &TrajInfo::callback, this);

  // Check if there is any publisher
  QFuture<void> future = QtConcurrent::run(this, &TrajInfo::checkForPublishers);
}

TrajInfo::~TrajInfo()
{
}

void TrajInfo::checkForPublishers()
{
  Q_EMIT setEnabled(false);
  ros::Duration(0.5).sleep();

  while (nh_.ok())
  {
    if (sub_.getNumPublishers() != 0)
    {
      ROS_INFO_STREAM(
          "RViz panel " << getName().toStdString() << " topic " << sub_.getTopic() << " has at least one publisher");
      break;
    }
    else
    {
      ROS_ERROR_STREAM(
          "RViz panel " << getName().toStdString() << " topic " << sub_.getTopic() << " has zero publishers!");
      ros::Duration(1).sleep();
    }
  }

  Q_EMIT setEnabled(true);
}

void TrajInfo::callback(const ram_msgs::AdditiveManufacturingTrajectoryInfoConstPtr& msg)
{
  std::lock_guard<std::recursive_mutex> lock(msg_mutex_);
  msg_ = *msg;
  Q_EMIT updateGUIFromParameters();
}

void TrajInfo::updateGUIFromParameters()
{
  std::lock_guard<std::recursive_mutex> lock(msg_mutex_);
  QDateTime time;
  time.setTime_t(uint(msg_.generated.toSec()));
  generated_->setText(time.toString("dd/MM/yyyy - hh:mm:ss"));
  time.setTime_t(uint(msg_.modified.toSec()));
  modified_->setText(time.toString("dd/MM/yyyy - hh:mm:ss"));
  similar_layers_->setText(msg_.similar_layers ? "Yes" : "No");
  number_of_layers_levels_->setText(QString::number(msg_.number_of_layers_levels));
  number_of_layers_indices_->setText(QString::number(msg_.number_of_layers_indices));
  number_of_polygons_->setText(QString::number(msg_.number_of_polygons));
  number_of_poses_->setText(QString::number(msg_.number_of_poses));
  trajectory_length_->setText(QString::number(msg_.trajectory_length, 'f', 3) + " meters");
  execution_time_->setText(QString::number(msg_.execution_time / 60.0, 'f', 1) + " minutes");
  wire_length_->setText(QString::number(msg_.wire_length, 'f', 1) + " meters");
  powder_weight_->setText(QString::number(msg_.powder_weight, 'f', 1) + " kilograms");
}

void TrajInfo::load(const rviz::Config& config)
{
  rviz::Panel::load(config);
}

void TrajInfo::save(rviz::Config config) const
                    {
  rviz::Panel::save(config);
}

}

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(ram_qt_guis::TrajInfo, rviz::Panel)
