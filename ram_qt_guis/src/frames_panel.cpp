#include <ram_qt_guis/frames_panel.hpp>

namespace ram_qt_guis
{
Frames::Frames(QWidget* parent) :
        rviz::Panel(parent)
{
  this->setObjectName("Frame");

  QVBoxLayout* main_layout = new QVBoxLayout(this);

  trajectory_frame_ = new Pose(QString::fromStdString("trajectory_frame"));
  trajectory_frame_->setText("Enter the robot trajectory frame:");
  start_pose_ = new Pose(QString::fromStdString("start_pose"));
  start_pose_->setText("Enter the start pose:");

  main_layout->addLayout(trajectory_frame_->getLayout());
  main_layout->addStretch(1);
  main_layout->addLayout(start_pose_->getLayout());
  main_layout->addStretch(2);

  connect(this, SIGNAL(displayErrorMessageBox(const QString, const QString, const QString)), this,
          SLOT(displayErrorBoxHandler(const QString, const QString, const QString)));

  trajectory_frame_pub_ = nh_.advertise<geometry_msgs::Pose>("ram/trajectory_frame", 1);
  start_pose_pub_ = nh_.advertise<geometry_msgs::Pose>("ram/start_pose", 1);

  trajectory_frame_pose_.orientation.w = 1;
  start_pose_pose_.orientation.w = 1;
}

Frames::~Frames()
{
}

void Frames::updateInternalParameters()
{
  tf::poseEigenToMsg(trajectory_frame_->getPose(), trajectory_frame_pose_);
  tf::poseEigenToMsg(start_pose_->getPose(), start_pose_pose_);
}

void Frames::sendTrajectoryFrameInformation()
{
  updateInternalParameters();
  if (trajectory_frame_pub_.getNumSubscribers() == 0)
  {
    Q_EMIT displayErrorMessageBox(
                                  "Published message will be lost: ",
                                  QString::fromStdString(trajectory_frame_pub_.getTopic() + " has no subscriber"), "");
  }
  trajectory_frame_pub_.publish(trajectory_frame_pose_);
  ros::spinOnce();
}

void Frames::sendStartPoseInformation()
{
  updateInternalParameters();
  if (start_pose_pub_.getNumSubscribers() == 0)
  {
    Q_EMIT displayErrorMessageBox(
                                  "Published message will be lost: ",
                                  QString::fromStdString(start_pose_pub_.getTopic() + " has no subscriber"), "");
  }
  start_pose_pub_.publish(start_pose_pose_);
  ros::spinOnce();
}

void Frames::load(const rviz::Config& config)
{
  trajectory_frame_->load(config);
  start_pose_->load(config);
  rviz::Panel::load(config);
  QFuture<void> future = QtConcurrent::run(this, &Frames::sendLoadedInformation);
}

void Frames::sendLoadedInformation()
{
  updateInternalParameters();
  Q_EMIT setEnabled(false);
  ros::Duration(0.2).sleep();

  // Send parameters
  while (1)
  {
    ros::Duration(0.1).sleep();

    if (trajectory_frame_pub_.getNumSubscribers() != 0)
    {
      trajectory_frame_pub_.publish(trajectory_frame_pose_);
      ros::spinOnce();
      break;
    }
    else
      ROS_WARN_STREAM_THROTTLE(1, "Topic " << trajectory_frame_pub_.getTopic() << " has zero subscriber");
  }

  while (1)
  {
    ros::Duration(0.1).sleep();

    if (start_pose_pub_.getNumSubscribers() != 0)
    {
      start_pose_pub_.publish(start_pose_pose_);
      ros::spinOnce();
      break;
    }
    else
      ROS_WARN_STREAM_THROTTLE(1, "Topic " << start_pose_pub_.getTopic() << " has zero subscriber");
  }

  connect(trajectory_frame_, SIGNAL(valueChanged()), this, SLOT(sendTrajectoryFrameInformation()));
  connect(start_pose_, SIGNAL(valueChanged()), this, SLOT(sendStartPoseInformation()));
  Q_EMIT setEnabled(true);
}

void Frames::save(rviz::Config config) const
                  {
  trajectory_frame_->save(config);
  start_pose_->save(config);
  rviz::Panel::save(config);
}

void Frames::displayErrorBoxHandler(const QString title,
                                    const QString message,
                                    const QString info_msg)
{
  Q_EMIT setEnabled(false);
  QMessageBox msg_box;
  msg_box.setWindowTitle(title);
  msg_box.setText(message);
  msg_box.setInformativeText(info_msg);
  msg_box.setIcon(QMessageBox::Critical);
  msg_box.setStandardButtons(QMessageBox::Ok);
  msg_box.exec();
  Q_EMIT setEnabled(true);
}

}

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(ram_qt_guis::Frames, rviz::Panel)
