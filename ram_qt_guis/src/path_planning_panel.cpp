#include <ram_qt_guis/path_planning_panel.hpp>

namespace ram_qt_guis
{
const double default_height_between_layers(1);
const double default_deposited_material_width_(1);
const double default_contours_filtering_tolerance(1);

const double default_slicing_direction_x(0);
const double default_slicing_direction_y(0);
const double default_slicing_direction_z(1);

PathPlanning::PathPlanning(QWidget* parent) :
        rviz::Panel(parent)
{
  this->setObjectName("PathPlanning");

  QHBoxLayout *file = new QHBoxLayout;
  QPushButton *file_explorer = new QPushButton;
  file_explorer->setText("...");
  file_explorer->setMaximumSize(30, 30);
  file_ = new QLineEdit;
  connect(file_, SIGNAL(textChanged(QString)), this, SLOT(fileChanged()));
  file->addWidget(file_);
  file->addWidget(file_explorer);
  connect(file_explorer, SIGNAL(released()), this, SLOT(browseFiles()));

  number_of_layers_widget_ = new QWidget;
  QVBoxLayout *number_of_layers_layout = new QVBoxLayout;
  number_of_layers_widget_->setLayout(number_of_layers_layout);

  number_of_layers_ = new QSpinBox;
  number_of_layers_->setRange(1, 10000);

  number_of_layers_layout->addWidget(new QLabel("Number of layers:"));
  number_of_layers_layout->addWidget(number_of_layers_);

  height_between_layers_ = new QDoubleSpinBox;
  height_between_layers_->setRange(0.001, 1000);
  height_between_layers_->setSuffix(" mm");
  height_between_layers_->setSingleStep(0.1);
  height_between_layers_->setDecimals(3);
  height_between_layers_->setValue(default_height_between_layers);

  deposited_material_width_ = new QDoubleSpinBox;
  deposited_material_width_->setRange(0.001, 1000);
  deposited_material_width_->setSuffix(" mm");
  deposited_material_width_->setSingleStep(0.1);
  deposited_material_width_->setDecimals(3);
  deposited_material_width_->setValue(default_height_between_layers);

  contours_filtering_tolerance_ = new QDoubleSpinBox;
  contours_filtering_tolerance_->setRange(0, 100);
  contours_filtering_tolerance_->setSuffix(" mm");
  contours_filtering_tolerance_->setSingleStep(0.1);
  contours_filtering_tolerance_->setDecimals(3);
  contours_filtering_tolerance_->setValue(default_contours_filtering_tolerance);

  slicing_direction_widget_ = new QWidget;
  QVBoxLayout *slicing_direction_layout = new QVBoxLayout;
  slicing_direction_widget_->setLayout(slicing_direction_layout);
  slicing_direction_layout->addWidget(new QLabel("Slicing direction:"));

  slicing_direction_x_ = new QDoubleSpinBox;
  slicing_direction_x_->setRange(-20000, 20000);
  slicing_direction_x_->setSingleStep(0.01);
  slicing_direction_x_->setDecimals(3);
  slicing_direction_y_ = new QDoubleSpinBox;
  slicing_direction_y_->setRange(-20000, 20000);
  slicing_direction_y_->setSingleStep(0.01);
  slicing_direction_y_->setDecimals(3);
  slicing_direction_z_ = new QDoubleSpinBox;
  slicing_direction_z_->setRange(-20000, 20000);
  slicing_direction_z_->setSingleStep(0.01);
  slicing_direction_z_->setDecimals(3);
  QLabel* label_slicing_dir_x = new QLabel("X");
  //label_pose_x->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
  QLabel* label_slicing_dir_y = new QLabel("Y");
  QLabel* label_slicing_dir_z = new QLabel("Z");
  QGridLayout* vector_layout = new QGridLayout;
  vector_layout->addWidget(label_slicing_dir_x, 0, 0);
  vector_layout->addWidget(slicing_direction_x_, 1, 0);
  vector_layout->addWidget(label_slicing_dir_y, 0, 1);
  vector_layout->addWidget(slicing_direction_y_, 1, 1);
  vector_layout->addWidget(label_slicing_dir_z, 0, 2);
  vector_layout->addWidget(slicing_direction_z_, 1, 2);
  slicing_direction_layout->addLayout(vector_layout);

  generate_trajectory_button_ = new QPushButton("Generate trajectory");
  connect(generate_trajectory_button_, SIGNAL(clicked()), this, SLOT(sendInformationButtonHandler()));

  QVBoxLayout *scroll_widget_layout = new QVBoxLayout();
  QWidget *scroll_widget = new QWidget;
  scroll_widget->setLayout(scroll_widget_layout);
  QScrollArea *scroll_area = new QScrollArea;
  scroll_area->setWidget(scroll_widget);
  scroll_area->setWidgetResizable(true);
  scroll_area->setFrameShape(QFrame::NoFrame);
  QVBoxLayout* main_layout = new QVBoxLayout(this);
  main_layout->addWidget(scroll_area);

  scroll_widget_layout->addWidget(new QLabel("YAML or mesh file:"));
  scroll_widget_layout->addLayout(file);
  scroll_widget_layout->addWidget(number_of_layers_widget_);
  scroll_widget_layout->addWidget(new QLabel("Height between layers:"));
  scroll_widget_layout->addWidget(height_between_layers_);
  scroll_widget_layout->addWidget(new QLabel("Deposited material width:"));
  scroll_widget_layout->addWidget(deposited_material_width_);
  scroll_widget_layout->addWidget(new QLabel("Contours filtering tolerance:"));
  scroll_widget_layout->addWidget(contours_filtering_tolerance_);
  scroll_widget_layout->addWidget(slicing_direction_widget_);
  scroll_widget_layout->addStretch(1);

  main_layout->addWidget(generate_trajectory_button_);

  connect(this, SIGNAL(displayErrorMessageBox(const QString, const QString, const QString)), this,
          SLOT(displayErrorBoxHandler(const QString, const QString, const QString)));

  // Setup service clients
  generate_trajectory_client_ = nh_.serviceClient<ram_path_planning::GenerateTrajectory>(
      "ram/path_planning/generate_trajectory");

  // Check connection of client
  QFuture<void> future = QtConcurrent::run(this, &PathPlanning::connectToServices);
}

PathPlanning::~PathPlanning()
{
}

void PathPlanning::connectToServices()
{
  Q_EMIT setEnabled(false);

  while (nh_.ok())
  {
    if (generate_trajectory_client_.waitForExistence(ros::Duration(2)))
    {
      ROS_INFO_STREAM(
          "RViz panel " << getName().toStdString() << " connected to the service " << generate_trajectory_client_.getService());
      break;
    }
    else
    {
      ROS_ERROR_STREAM(
          "RViz panel " << getName().toStdString() << " could not connect to ROS service: " << generate_trajectory_client_.getService());
      ros::Duration(1).sleep();
    }
  }

  ROS_INFO_STREAM("RViz panel " << getName().toStdString() << " services connections have been made");
  Q_EMIT setEnabled(true);
}

void PathPlanning::displayErrorBoxHandler(const QString title,
                                          const QString message,
                                          const QString info_msg)
{
  Q_EMIT setEnabled(false);
  QMessageBox msg_box;
  msg_box.setWindowTitle(title);
  msg_box.setText(message);
  msg_box.setInformativeText(info_msg);
  msg_box.setIcon(QMessageBox::Critical);
  msg_box.setStandardButtons(QMessageBox::Ok);
  msg_box.exec();
  Q_EMIT setEnabled(true);
}

void PathPlanning::browseFiles()
{
  QString file_dir("");
  {
    QFileInfo file(file_->text());
    if (!file_->text().isEmpty() && file.dir().exists())
      file_dir = file.dir().path();
    else
    {
      std::string path = ros::package::getPath("ram_path_planning");
      file_dir = QString::fromStdString(path);
    }
  }

  QFileDialog browser;
  QString file_path = browser.getOpenFileName(0, tr("Choose YAML or mesh file"), file_dir,
                                              tr("YAML and mesh files (*.yaml *.yml *.ply *.stl *.obj)"));
  if (file_path != "")
    file_->setText(file_path);
}

void PathPlanning::fileChanged()
{
  std::string file_extension(fileExtension(file_->text().toStdString()));
  if (!strcasecmp(file_extension.c_str(), "yaml") || !strcasecmp(file_extension.c_str(), "yml"))
  // YAML
  {
    number_of_layers_widget_->setEnabled(true);
    slicing_direction_widget_->setEnabled(false);
  }
  else if (!strcasecmp(file_extension.c_str(), "ply") || !strcasecmp(file_extension.c_str(), "stl")
      || !strcasecmp(file_extension.c_str(), "obj"))
  // Mesh
  {
    number_of_layers_widget_->setEnabled(false);
    slicing_direction_widget_->setEnabled(true);
  }
  else
  // Enable everything
  {
    number_of_layers_widget_->setEnabled(true);
    slicing_direction_widget_->setEnabled(true);
  }
}

void PathPlanning::sendInformationButtonHandler()
{
  Q_EMIT configChanged();

  // Check that file exists
  QFileInfo file(file_->text());
  if (!file.exists())
  {
    Q_EMIT displayErrorMessageBox("Error", "YAML or mesh file does not exist", "Choose another file");
    return;
  }

  Q_EMIT setEnabled(false);

  generate_trajectory_.request.generation_params.deposited_material_width = deposited_material_width_->value() / 1000.0;
  generate_trajectory_.request.generation_params.contours_filtering_tolerance = contours_filtering_tolerance_->value() / 1000.0;
  generate_trajectory_.request.generation_params.file = file_->text().toStdString();
  generate_trajectory_.request.generation_params.height_between_layers = height_between_layers_->value() / 1000.0;
  generate_trajectory_.request.generation_params.number_of_layers = number_of_layers_->value();

  if (slicing_direction_x_->value() == 0 && slicing_direction_y_->value() == 0 && slicing_direction_z_->value() == 0)
    slicing_direction_z_->setValue(1);

  generate_trajectory_.request.generation_params.slicing_direction.x = slicing_direction_x_->value();
  generate_trajectory_.request.generation_params.slicing_direction.y = slicing_direction_y_->value();
  generate_trajectory_.request.generation_params.slicing_direction.z = slicing_direction_z_->value();

  // Run in a separate thread
  QFuture<void> future = QtConcurrent::run(this, &PathPlanning::sendInformation);
}

void PathPlanning::sendInformation()
{
  // Call service
  bool success(generate_trajectory_client_.call(generate_trajectory_));

  if (!success)
  {
    Q_EMIT displayErrorMessageBox("Service call failed",
                                  QString::fromStdString(generate_trajectory_client_.getService()),
                                  "Check the logs!");
    return;
  }

  if(!generate_trajectory_.response.error_msg.empty())
  {
    Q_EMIT displayErrorMessageBox("Path planning failed",
                                  QString::fromStdString(generate_trajectory_.response.error_msg),
                                  "");
    return;
  }

  Q_EMIT setEnabled(true);
}

void PathPlanning::load(const rviz::Config& config)
{
  QString tmp_str("");
  int tmp_int(0);
  float tmp_float(0.01);
  Q_EMIT configChanged();
  if (config.mapGetString("file", &tmp_str))
    file_->setText(tmp_str);

  if (config.mapGetInt("number_of_layers", &tmp_int))
    number_of_layers_->setValue(tmp_int);

  if (config.mapGetFloat("height_between_layers", &tmp_float))
    height_between_layers_->setValue(tmp_float);
  else
    height_between_layers_->setValue(default_height_between_layers);

  if (config.mapGetFloat("deposited_material_width", &tmp_float))
    deposited_material_width_->setValue(tmp_float);
  else
    deposited_material_width_->setValue(default_deposited_material_width_);

  if (config.mapGetFloat("contours_filtering_tolerance", &tmp_float))
    contours_filtering_tolerance_->setValue(tmp_float);
  else
    contours_filtering_tolerance_->setValue(default_contours_filtering_tolerance);

  if (config.mapGetFloat("slicing_direction_x", &tmp_float))
    slicing_direction_x_->setValue(tmp_float);
  else
    slicing_direction_x_->setValue(default_slicing_direction_x);
  if (config.mapGetFloat("slicing_direction_y", &tmp_float))
    slicing_direction_y_->setValue(tmp_float);
  else
    slicing_direction_y_->setValue(default_slicing_direction_y);
  if (config.mapGetFloat("slicing_direction_z", &tmp_float))
    slicing_direction_z_->setValue(tmp_float);
  else
    slicing_direction_z_->setValue(default_slicing_direction_z);

  rviz::Panel::load(config);
}

void PathPlanning::save(rviz::Config config) const
                        {
  config.mapSetValue("file", file_->text());
  config.mapSetValue("number_of_layers", number_of_layers_->value());
  config.mapSetValue("height_between_layers", height_between_layers_->value());
  config.mapSetValue("deposited_material_width", deposited_material_width_->value());
  config.mapSetValue("contours_filtering_tolerance", contours_filtering_tolerance_->value());
  config.mapSetValue("slicing_direction_x", slicing_direction_x_->value());
  config.mapSetValue("slicing_direction_y", slicing_direction_y_->value());
  config.mapSetValue("slicing_direction_z", slicing_direction_z_->value());
  rviz::Panel::save(config);
}

std::string PathPlanning::fileExtension(const std::string full_path)
{
  size_t last_index = full_path.find_last_of("/");
  std::string file_name = full_path.substr(last_index + 1, full_path.size());

  last_index = file_name.find_last_of("\\");
  file_name = file_name.substr(last_index + 1, file_name.size());

  last_index = file_name.find_last_of(".");
  if (last_index == std::string::npos)
    return "";

  return file_name.substr(last_index + 1, file_name.size());
}

}

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(ram_qt_guis::PathPlanning, rviz::Panel)
