#include <ram_qt_guis/trajectory_buffer_panel.hpp>

namespace ram_qt_guis
{
TrajectoryBuffer::TrajectoryBuffer(QWidget* parent) :
        rviz::Panel(parent)
{
  this->setObjectName("Trajectory buffer");

  QSizePolicy policy(QSizePolicy::Expanding, QSizePolicy::Expanding);
  back_button_ = new QPushButton("Back");
  back_button_->setSizePolicy(policy);
  forward_button_ = new QPushButton("Forward");
  forward_button_->setSizePolicy(policy);

  QHBoxLayout* main_layout = new QHBoxLayout(this);
  main_layout->addWidget(back_button_);
  main_layout->addWidget(forward_button_);

  connect(back_button_, SIGNAL(clicked()), this, SLOT(backButtonHandler()));
  connect(forward_button_, SIGNAL(clicked()), this, SLOT(forwardButtonHandler()));
  connect(this, SIGNAL(displayErrorMessageBox(const QString, const QString, const QString)), this,
          SLOT(displayErrorBoxHandler(const QString, const QString, const QString)));

  trajectory_buffer_client_ = nh_.serviceClient<ram_trajectory_buffer::BufferParams>("ram/buffer/get_trajectory");

  // Check connection of client
  QFuture<void> future = QtConcurrent::run(this, &TrajectoryBuffer::connectToServices);
}

TrajectoryBuffer::~TrajectoryBuffer()
{
}

void TrajectoryBuffer::connectToServices()
{
  Q_EMIT setEnabled(false);
  while (nh_.ok())
  {
    if (trajectory_buffer_client_.waitForExistence(ros::Duration(2)))
    {
      ROS_INFO_STREAM(
          "RViz panel " << getName().toStdString() << " connected to the service " << trajectory_buffer_client_.getService());
      break;
    }
    else
    {
      ROS_ERROR_STREAM(
          "RViz panel " << getName().toStdString() << " could not connect to ROS service: " << trajectory_buffer_client_.getService());
      ros::Duration(1).sleep();
    }
  }
  Q_EMIT setEnabled(true);
}

void TrajectoryBuffer::backButtonHandler()
{
  Q_EMIT configChanged();
  Q_EMIT setEnabled(false);
  params_.request.button_id = 1;
  // Run in a separate thread
  QFuture<void> future = QtConcurrent::run(this, &TrajectoryBuffer::sendButton);
}

void TrajectoryBuffer::forwardButtonHandler()
{
  Q_EMIT configChanged();
  Q_EMIT setEnabled(false);
  params_.request.button_id = 2;
  // Run in a separate thread
  QFuture<void> future = QtConcurrent::run(this, &TrajectoryBuffer::sendButton);
}

void TrajectoryBuffer::sendButton()
{
  // Call service
  bool success(trajectory_buffer_client_.call(params_));

  if (!success)
  {
    Q_EMIT displayErrorMessageBox("Service call failed", QString::fromStdString(trajectory_buffer_client_.getService()),
                                  "Check the logs!");
  }

  if (!params_.response.error.empty())
  {
    Q_EMIT displayErrorMessageBox(QString::fromStdString(trajectory_buffer_client_.getService()),
                                  QString::fromStdString(params_.response.error),
                                  "");
  }

  Q_EMIT setEnabled(true);
}

void TrajectoryBuffer::load(const rviz::Config& config)
{
  rviz::Panel::load(config);
}

void TrajectoryBuffer::save(rviz::Config config) const
                            {
  rviz::Panel::save(config);
}

void TrajectoryBuffer::displayErrorBoxHandler(const QString title,
                                              const QString message,
                                              const QString info_msg)
{
  Q_EMIT setEnabled(false);
  QMessageBox msg_box;
  msg_box.setWindowTitle(title);
  msg_box.setText(message);
  msg_box.setInformativeText(info_msg);
  msg_box.setIcon(QMessageBox::Critical);
  msg_box.setStandardButtons(QMessageBox::Ok);
  msg_box.exec();
  Q_EMIT setEnabled(true);
}

}

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(ram_qt_guis::TrajectoryBuffer, rviz::Panel)
