#ifndef PATH_PLANNING_PANEL_HPP
#define PATH_PLANNING_PANEL_HPP

#ifndef Q_MOC_RUN
#include <eigen_conversions/eigen_msg.h>
#include <ram_path_planning/GenerateTrajectory.h>
#include <ros/package.h>
#include <ros/ros.h>
#include <ros/service.h>
#include <rviz/panel.h>
#endif

#include <QComboBox>
#include <QDoubleSpinBox>
#include <QFileDialog>
#include <QFuture>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QScrollArea>
#include <QtConcurrent/QtConcurrentRun>

namespace ram_qt_guis
{
class PathPlanning : public rviz::Panel
{
Q_OBJECT

public:
  PathPlanning(QWidget* parent = NULL);
  virtual ~PathPlanning();
  void connectToServices();

Q_SIGNALS:
  void displayErrorMessageBox(const QString,
                              const QString,
                              const QString);

protected Q_SLOTS:
  void browseFiles();
  void fileChanged();
  void sendInformationButtonHandler();
  virtual void sendInformation();

  void displayErrorBoxHandler(const QString title,
                              const QString message,
                              const QString info_msg);

  virtual void load(const rviz::Config& config);
  virtual void save(rviz::Config config) const;

protected:
  std::string fileExtension(const std::string full_path);

  QLineEdit *file_;
  QWidget * number_of_layers_widget_;
  QSpinBox *number_of_layers_;
  QDoubleSpinBox *height_between_layers_;
  QDoubleSpinBox *deposited_material_width_;
  QDoubleSpinBox *contours_filtering_tolerance_;
  QPushButton *generate_trajectory_button_;

  QWidget * slicing_direction_widget_;
  QDoubleSpinBox *slicing_direction_x_;
  QDoubleSpinBox *slicing_direction_y_;
  QDoubleSpinBox *slicing_direction_z_;

  ros::NodeHandle nh_;
  ros::ServiceClient generate_trajectory_client_;
  ram_path_planning::GenerateTrajectory generate_trajectory_;
};

}

#endif
