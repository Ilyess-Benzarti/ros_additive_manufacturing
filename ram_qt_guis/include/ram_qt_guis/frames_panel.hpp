#ifndef TRAJECTORY_FRAME_PANEL_HPP
#define TRAJECTORY_FRAME_PANEL_HPP

#ifndef Q_MOC_RUN
#include <eigen_conversions/eigen_msg.h>
#include <ram_qt_guis/pose_widget.hpp>
#include <ros/ros.h>
#include <ros/service.h>
#include <rviz/panel.h>
#endif

#include <QDoubleSpinBox>
#include <QFuture>
#include <QHBoxLayout>
#include <QLabel>
#include <QMessageBox>
#include <QPushButton>
#include <QtConcurrent/QtConcurrentRun>

namespace ram_qt_guis
{
class Frames : public rviz::Panel
{
Q_OBJECT
  public:
  Frames(QWidget* parent = NULL);
  virtual ~Frames();

Q_SIGNALS:
  void displayErrorMessageBox(const QString,
                              const QString,
                              const QString);

private:
  void updateInternalParameters();

protected Q_SLOTS:
  void sendInformationButtonHandler();
  virtual void sendTrajectoryFrameInformation();
  void sendStartPoseInformation();

  virtual void load(const rviz::Config& config);
  virtual void sendLoadedInformation();
  virtual void save(rviz::Config config) const;

  void displayErrorBoxHandler(const QString title,
                              const QString message,
                              const QString info_msg);

protected:
  Pose *trajectory_frame_;
  Pose *start_pose_;

  ros::NodeHandle nh_;
  ros::Publisher trajectory_frame_pub_;
  geometry_msgs::Pose trajectory_frame_pose_;
  ros::Publisher start_pose_pub_;
  geometry_msgs::Pose start_pose_pose_;
};

}

#endif
