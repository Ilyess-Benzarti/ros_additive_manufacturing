#ifndef MODIFY_PANEL_HPP
#define MODIFY_PANEL_HPP

#ifndef Q_MOC_RUN
#include <mutex>

#include <eigen_conversions/eigen_msg.h>
#include <ros/ros.h>
#include <ros/service.h>
#include <rviz/panel.h>
#include <ram_display/UpdateSelection.h>
#include <ram_modify_trajectory/AddPoses.h>
#include <ram_modify_trajectory/DeleteSelectedPoses.h>
#include <ram_modify_trajectory/GetPosesFromLayer.h>
#include <ram_modify_trajectory/GetPosesFromLayersList.h>
#include <ram_modify_trajectory/GetPosesFromTrajectory.h>
#include <ram_modify_trajectory/ModifySelectedPoses.h>
#include <ram_modify_trajectory/ResetSelectedPoses.h>
#include <ram_msgs/AdditiveManufacturingTrajectory.h>
#include <ram_utils/GetLayerSize.h>
#include <ram_utils/GetNumberOfLayersLevels.h>
#include <ram_utils/GetTrajectorySize.h>
#endif

#include <QCheckBox>
#include <QComboBox>
#include <QDialogButtonBox>
#include <QLabel>
#include <QMessageBox>
#include <QPushButton>
#include <QRadioButton>
#include <QScrollArea>
#include <QSpinBox>
#include <QTabWidget>
#include <QVBoxLayout>
#include <QtConcurrent/QtConcurrentRun>

#include <ram_qt_guis/modify_panel_modify_poses.hpp>
#include <ram_qt_guis/modify_panel_range_list_selection.hpp>

namespace ram_qt_guis
{
class Modify : public rviz::Panel
{
Q_OBJECT
  public:
  Modify(QWidget* parent = NULL);
  virtual ~Modify();
  void connectToService(ros::ServiceClient &client);
  void connectToServices();

Q_SIGNALS:
  void displayErrorMessageBox(const QString,
                              const QString,
                              const QString);

private:
  void trajReceived(const ram_msgs::AdditiveManufacturingTrajectory::Ptr& msg);

  void clearLayout(QLayout* layout,
                   bool delete_widgets = true);
  void changeGUIToSelectionMode();
  void changeGUIToRangeListSelection(const QString help_string,
                                     const unsigned min,
                                     const unsigned max);

  void changeGUIToOperationSelection();

  void changeGUIToModifyPoses();

  void getSelection();

protected Q_SLOTS:
  void selectionModeSelected();
  void propagateSelection();
  void operationSelected();
  void modifyPoses();

  virtual void load(const rviz::Config& config);
  virtual void save(rviz::Config config) const;

  void displayErrorBoxHandler(const QString title,
                              const QString message,
                              const QString info_msg);

  void updateTemporarySelection(std::vector<unsigned> selection);

protected:
  QVBoxLayout *layout_;
  // UI for range list selection
  ModifyRangeListSelection *range_list_selection_ui_;

  // Selection mode
  QVector<QRadioButton*> selection_buttons_;

  // modify pose UI
  ModifyPoses *modify_poses_ui_;

  // Operation selection
  std::vector<QRadioButton *> operations_;
  std::vector<QRadioButton *> geometric_operations_;

  // Selection
  std::vector<ram_msgs::AdditiveManufacturingPose> selected_poses_;
  unsigned selection_mode_;
  unsigned layer_level_; // Only makes sense if selection mode is 2
  bool is_propagating_;  // Only makes sense if selection mode is 2
  std::vector<unsigned> layers_to_propagate_; // Only makes sense if selection mode is 2
  std::vector<unsigned> relative_indices_; // Only makes sense if selection mode is 2

  ros::NodeHandle nh_;

  // Subscribe to trajectory topic
  ros::Subscriber traj_;
  std::mutex trajectory_mutex_;
  ram_msgs::AdditiveManufacturingTrajectory trajectory_;

  // Selection visualization
  ros::ServiceClient update_selection_client_;

  // Pose selector services
  ros::ServiceClient get_layer_size_client_;
  ros::ServiceClient get_number_of_layers_client_;
  ros::ServiceClient get_poses_from_layer_client_;
  ros::ServiceClient get_poses_from_layers_list_client_;
  ros::ServiceClient get_poses_from_trajectory_client_;
  ros::ServiceClient get_trajectory_size_client_;

  // Modify services
  ros::ServiceClient modify_selected_poses_client_;
  ros::ServiceClient delete_selected_poses_client_;
  ros::ServiceClient add_poses_client_;
  ros::ServiceClient reset_selected_poses_client_;
};

}

#endif
