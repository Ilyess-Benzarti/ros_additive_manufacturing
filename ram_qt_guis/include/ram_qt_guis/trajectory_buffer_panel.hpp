#ifndef TRAJECTORY_BUFFER_PANEL_HPP
#define TRAJECTORY_BUFFER_PANEL_HPP

#ifndef Q_MOC_RUN
#include <eigen_conversions/eigen_msg.h>
#include <ram_trajectory_buffer/BufferParams.h>
#include <ros/ros.h>
#include <ros/service.h>
#include <rviz/panel.h>
#endif

#include <QFuture>
#include <QVBoxLayout>
#include <QLabel>
#include <QMessageBox>
#include <QPushButton>
#include <QtConcurrent/QtConcurrentRun>

namespace ram_qt_guis
{
class TrajectoryBuffer : public rviz::Panel
{
Q_OBJECT
  public:
TrajectoryBuffer(QWidget* parent = NULL);
  virtual ~TrajectoryBuffer();
  void connectToServices();

Q_SIGNALS:
  void displayErrorMessageBox(const QString,
                              const QString,
                              const QString);

protected Q_SLOTS:
  void backButtonHandler();
  void forwardButtonHandler();
  virtual void sendButton();

  virtual void load(const rviz::Config& config);
  virtual void save(rviz::Config config) const;

  void displayErrorBoxHandler(const QString title,
                              const QString message,
                              const QString info_msg);

protected:
  QPushButton *back_button_;
  QPushButton *forward_button_;

  ros::NodeHandle nh_;
  ram_trajectory_buffer::BufferParams params_;
  ros::ServiceClient trajectory_buffer_client_;
};

}

#endif
