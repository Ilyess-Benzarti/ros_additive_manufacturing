#ifndef ENTRY_EXIT_STRATEGIES_PANEL_HPP
#define ENTRY_EXIT_STRATEGIES_PANEL_HPP

#ifndef Q_MOC_RUN
#include <eigen_conversions/eigen_msg.h>
#include <ram_utils/EntryExitParameters.h>
#include <ros/package.h>
#include <ros/ros.h>
#include <ros/service.h>
#include <rviz/panel.h>
#endif

#include <QComboBox>
#include <QDoubleSpinBox>
#include <QFileDialog>
#include <QFuture>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QScrollArea>
#include <QtConcurrent/QtConcurrentRun>

namespace ram_qt_guis
{
class EntryExitStrategies : public rviz::Panel
{
Q_OBJECT

public:
  EntryExitStrategies(QWidget* parent = NULL);
  virtual ~EntryExitStrategies();

  void connectToServices();

Q_SIGNALS:
  void displayErrorMessageBox(const QString,
                              const QString,
                              const QString);

private:
  void updateInternalParameters();

protected Q_SLOTS:
  virtual void sendEntryParameters();

  virtual void sendExitParameters();

  void displayErrorBoxHandler(const QString title,
                              const QString message,
                              const QString info_msg);

  virtual void load(const rviz::Config& config);
  virtual void save(rviz::Config config) const;

protected:

  QSpinBox *entry_number_of_poses_;
  QDoubleSpinBox *entry_angle_;
  QDoubleSpinBox *entry_distance_;
  QPushButton *entry_button_;

  QSpinBox *exit_number_of_poses_;
  QDoubleSpinBox *exit_angle_;
  QDoubleSpinBox *exit_distance_;
  QPushButton *exit_button_;

  ros::NodeHandle nh_;
  ros::ServiceClient entry_parameters_client_;
  ros::ServiceClient exit_parameters_client_;

  ram_utils::EntryExitParameters entry_parameters_;
  ram_utils::EntryExitParameters exit_parameters_;
};

}

#endif
