#ifndef POSE_INFO_PANEL_HPP
#define POSE_INFO_PANEL_HPP

#ifndef Q_MOC_RUN
#include <eigen_conversions/eigen_msg.h>
#include <mutex>
#include <ram_modify_trajectory/GetPosesFromTrajectory.h>
#include <ram_msgs/AdditiveManufacturingTrajectory.h>
#include <ros/ros.h>
#include <rviz/panel.h>
#endif

#include <QDateTime>
#include <QHBoxLayout>
#include <QLabel>
#include <QMessageBox>
#include <QPushButton>
#include <QScrollArea>
#include <QSpinBox>
#include <QVBoxLayout>
#include <QtConcurrent/QtConcurrentRun>

namespace ram_qt_guis
{
class PoseInfo : public rviz::Panel
{
Q_OBJECT
  public:
  PoseInfo(QWidget* parent = NULL);
  virtual ~PoseInfo();

  void connectToServices();
  void checkForPublishers();
  void trajectoryCallback(const ram_msgs::AdditiveManufacturingTrajectoryConstPtr&);

protected Q_SLOTS:
  void backButtonHandler();
  void forwardButtonHandler();
  void firstPoseButtonHandler();
  void lastPoseButtonHandler();
  void getPoseInformation();
  void updateGUIparameters();

  virtual void load(const rviz::Config& config);
  virtual void save(rviz::Config config) const;

protected:
  unsigned trajectory_size_;

  // Geometric pose
  QLabel *x_;
  QLabel *y_;
  QLabel *z_;
  QLabel *w_;
  QLabel *p_;
  QLabel *r_;

  // Info
  QLabel *layers_level_;
  QLabel *layer_index_;
  QLabel *polygon_start_;
  QLabel *polygon_end_;
  QLabel *entry_pose_;
  QLabel *exit_pose_;

  // Parameters
  QLabel *movement_type_;
  QLabel *approach_type_;
  QLabel *blend_radius_;
  QLabel *speed_;
  QLabel *laser_power_;
  QLabel *feed_rate_;

  // Buttons
  QPushButton *back_button_;
  QPushButton *forward_button_;
  QPushButton *first_pose_button_;
  QPushButton *last_pose_button_;

  // Index
  QSpinBox *pose_index_;

  // ROS
  ros::NodeHandle nh_;
  ros::ServiceClient get_poses_from_trajectory_client_;
  ros::Subscriber trajectory_sub_;
  std::recursive_mutex pose_params_mutex_;
  ram_modify_trajectory::GetPosesFromTrajectory pose_params_;
};

}

#endif
