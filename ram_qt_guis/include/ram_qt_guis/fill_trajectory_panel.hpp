#ifndef FILL_TRAJECTORY_PANEL_HPP
#define FILL_TRAJECTORY_PANEL_HPP

#ifndef Q_MOC_RUN
#include <eigen_conversions/eigen_msg.h>
#include <ram_msgs/AdditiveManufacturingParams.h>
#include <ros/ros.h>
#include <ros/service.h>
#include <rviz/panel.h>
#endif

#include <QComboBox>
#include <QDoubleSpinBox>
#include <QFuture>
#include <QHBoxLayout>
#include <QLabel>
#include <QMessageBox>
#include <QPushButton>
#include <QScrollArea>
#include <QtConcurrent/QtConcurrentRun>

namespace ram_qt_guis
{
class FillTrajectory : public rviz::Panel
{
Q_OBJECT
  public:
  FillTrajectory(QWidget* parent = NULL);
  virtual ~FillTrajectory();

Q_SIGNALS:
  void displayErrorMessageBox(const QString,
                              const QString,
                              const QString);

private:
  void updateInternalParameters();

protected Q_SLOTS:
  void sendInformationButtonHandler();
  virtual void sendInformation();

  virtual void load(const rviz::Config& config);
  virtual void sendLoadedInformation();
  virtual void save(rviz::Config config) const;

  void displayErrorBoxHandler(const QString title,
                              const QString message,
                              const QString info_msg);

protected:
  QComboBox *movement_type_;
  QComboBox *approach_type_;
  QSpinBox *laser_power_;
  QDoubleSpinBox *feed_rate_;
  QSpinBox *blend_radius_;
  QDoubleSpinBox *speed_;
  QPushButton *send_button_;

  ros::NodeHandle nh_;
  ros::Publisher pub_;
  ram_msgs::AdditiveManufacturingParams params_;
};

}

#endif
